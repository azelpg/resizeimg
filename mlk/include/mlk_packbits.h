/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_PACKBITS_H
#define MLK_PACKBITS_H

typedef struct _mPackBits mPackBits;

struct _mPackBits
{
	uint8_t *buf,
		*workbuf;
	uint32_t bufsize,
		worksize,
		encsize;

	void *param;

	mlkerr (*readwrite)(mPackBits *p,uint8_t *buf,int size);
};


#ifdef __cplusplus
extern "C" {
#endif

mlkerr mPackBits_decode(mPackBits *p);
mlkerr mPackBits_encode(mPackBits *p);

#ifdef __cplusplus
}
#endif

#endif
