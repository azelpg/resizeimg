/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_BUFIO_H
#define MLK_BUFIO_H

struct _mBufIO
{
	uint8_t *cur,*top;
	mlksize bufsize;
	int endian;
};

enum MBUFIO_ENDIAN
{
	MBUFIO_ENDIAN_HOST = 0,
	MBUFIO_ENDIAN_LITTLE,
	MBUFIO_ENDIAN_BIG
};


#ifdef __cplusplus
extern "C" {
#endif

void mBufIO_init(mBufIO *p,void *buf,mlksize bufsize,int endian);

mlksize mBufIO_getPos(mBufIO *p);
mlksize mBufIO_getRemain(mBufIO *p);
int mBufIO_isRemain(mBufIO *p,mlksize size);

int mBufIO_seek(mBufIO *p,int32_t size);
int mBufIO_setPos(mBufIO *p,mlksize pos);
int mBufIO_setPos_get(mBufIO *p,mlksize pos,mlksize *plast);

int32_t mBufIO_read(mBufIO *p,void *buf,int32_t size);
int mBufIO_readOK(mBufIO *p,void *buf,int32_t size);
int mBufIO_readByte(mBufIO *p,void *buf);
int mBufIO_read16(mBufIO *p,void *buf);
int mBufIO_read32(mBufIO *p,void *buf);
int mBufIO_read16_array(mBufIO *p,void *buf,int cnt);
int mBufIO_read32_array(mBufIO *p,void *buf,int cnt);
uint16_t mBufIO_get16(mBufIO *p);
uint32_t mBufIO_get32(mBufIO *p);

int32_t mBufIO_write(mBufIO *p,const void *buf,int32_t size);
int mBufIO_write16(mBufIO *p,void *buf);
int mBufIO_write32(mBufIO *p,void *buf);
int mBufIO_set16(mBufIO *p,uint16_t val);
int mBufIO_set32(mBufIO *p,uint32_t val);

#ifdef __cplusplus
}
#endif

#endif
