/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_FILE_H
#define MLK_FILE_H

typedef int mFile;
typedef struct _mFileText mFileText;

#define MFILE_NONE  (-1)

enum MREADFILEFULL_FLAGS
{
	MREADFILEFULL_APPEND_0  = 1<<0,
	MREADFILEFULL_ACCEPT_EMPTY = 1<<1
};


#ifdef __cplusplus
extern "C" {
#endif

mlkerr mFileClose(mFile file);
mlkerr mFileOpen_read(mFile *file,const char *filename);
mlkerr mFileOpen_write(mFile *file,const char *filename,int perm);
mlkerr mFileOpen_temp(mFile *file,const char *filename);

mlkfoff mFileGetSize(mFile file);

mlkfoff mFileGetPos(mFile file);
mlkerr mFileSetPos(mFile file,mlkfoff pos);
mlkerr mFileSeekCur(mFile file,mlkfoff seek);
mlkerr mFileSeekEnd(mFile file,mlkfoff seek);

int32_t mFileRead(mFile file,void *buf,int32_t size);
mlkerr mFileRead_full(mFile file,void *buf,int32_t size);

int32_t mFileWrite(mFile file,const void *buf,int32_t size);
mlkerr mFileWrite_full(mFile file,const void *buf,int32_t size);

/*----*/

mlkbool mIsExistPath(const char *path);
mlkbool mIsExistFile(const char *path);
mlkbool mIsExistDir(const char *path);
mlkbool mGetFileStat(const char *path,mFileStat *dst);
mlkbool mGetFileSize(const char *path,mlkfoff *dst);
mlkbool mGetFileModifyTime(const char *path,int64_t *dst);
mlkerr mCreateDir(const char *path,int perm);
mlkerr mCreateDir_parents(const char *path,int perm);
mlkbool mDeleteFile(const char *path);
mlkbool mDeleteDir(const char *path);
int mCompareFileModify(const char *path1,const char *path2);

/* util */

mlkerr mReadFileFull_alloc(const char *filename,uint32_t flags,uint8_t **ppdst,int32_t *psize);
mlkerr mReadFileHead(const char *filename,void *buf,int32_t size);
mlkerr mCopyFile(const char *srcpath,const char *dstpath);
mlkerr mWriteFile_fromBuf(const char *filename,const void *buf,int32_t size);

mlkerr mFileText_readFile(mFileText **dst,const char *filename);
void mFileText_end(mFileText *p);
char *mFileText_nextLine(mFileText *p);
char *mFileText_nextLine_skip(mFileText *p);

#ifdef __cplusplus
}
#endif

#endif
