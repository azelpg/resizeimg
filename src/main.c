/*$
resizeimg
Copyright (c) 2016-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * main
 **********************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_charset.h>
#include <mlk_imagebuf.h>

#include "def.h"


//-----------------

OptionData g_opt;
WorkData g_work;

/* option.c */
int main_get_options(int argc,char **argv);

/* image.c */
void image_before_proc(mImageBuf2 *img);
void image_after_proc(mImageBuf2 *img);
mImageBuf2 *image_load(const char *filename);
int image_save(mImageBuf2 *img,const char *filename);

/* resize.c */
void ResizeParam_free(ResizeParam *p);
mImageBuf2 *image_resize(mImageBuf2 *imgsrc,int oldw,int oldh,int neww,int newh);

//-----------------



/* 画像サイズ取得
 *
 * - 拡大も可能。
 * - アルファ付き出力の場合、常に原寸大 (切り取りは有効)。
 * 
 * cropsize: 切り取り後のサイズ
 * outsize: リサイズ後のサイズ
 * return: 0 以外でエラー */

static int _get_imgsize(mImageBuf2 *img,mSize *cropsize,mSize *outsize)
{
	int sw,sh,dw,dh;

	//---- 切り取り後のサイズ (sw,sh)

	if(g_opt.flags & OPTION_F_CROP_SIZE)
	{
		//x2,y2 はサイズ

		sw = g_opt.rcCrop.x2;
		sh = g_opt.rcCrop.y2;
	}
	else
	{
		//切り取り幅

		sw = img->width - g_opt.rcCrop.x1 - g_opt.rcCrop.x2;
		sh = img->height - g_opt.rcCrop.y1 - g_opt.rcCrop.y2;
	}

	//幅のチェック

	if(sw <= 0 || sh <= 0
		|| g_opt.rcCrop.x1 + sw > img->width
		|| g_opt.rcCrop.y1 + sh > img->height)
	{
		putmes("(%dx%d) : crop size error\n", img->width, img->height);
		return 1;
	}

	//---- 出力サイズ
	//[*] --size,--width,--height が複数指定された場合は、最後のオプションが適用される。

	dw = g_opt.out_width;
	dh = g_opt.out_height;

	if(is_output_alpha())
	{
		//アルファ付き出力の場合は、常に原寸大

		dw = sw;
		dh = sh;
	}
	else if(dw == 0 && dh == 0 && g_opt.dscale == 0)
	{
		//すべて 0 なら、100% (デフォルト)

		dw = sw;
		dh = sh;
	}
	else if(g_opt.dscale != 0)
	{
		//% 指定 (--size N%)
		
		dw = (int)(sw * g_opt.dscale + 0.5);
		dh = (int)(sh * g_opt.dscale + 0.5);
	}
	else if(dh == 0)
		//幅指定、高さは自動 (--width)
		dh = (int)((double)dw / sw * sh + 0.5);
	else if(dw == 0)
		//高さ指定、幅は自動 (--height)
		dw = (int)((double)dh / sh * sw + 0.5);

	//(scale = 0, out_width,out_height != 0 の場合は、"--size WxH")

	//計算結果が 0 になる場合、1 に

	if(dw == 0) dw = 1;
	if(dh == 0) dh = 1;

	//出力の最大サイズ

	if(dw > IMAGESIZE_MAX || dh > IMAGESIZE_MAX)
	{
		putmes("(%dx%d): maximum size is " MLK_TO_STR_MACRO(IMAGESIZE_MAX) " px\n", dw, dh);
		return 1;
	}

	//WebP は出力サイズ 16383 px まで

	if(g_opt.out_format == FORMAT_WEBP
		&& (dw > 16383 || dh > 16383))
	{
		putmes("(%dx%d): maximum size of WebP is 16383 px\n", dw, dh);
		return 1;
	}

	//---- セット

	cropsize->w = sw, cropsize->h = sh;
	outsize->w = dw, outsize->h = dh;

	return 0;
}

/* 経過表示開始 */

static void _start_progress(int max)
{
	int i;

	if(!g_opt.putmes) return;

	g_work.prog_curcnt = 0;
	g_work.prog_curmark = 0;

	//各マーク時のカウント値
	// 0=10%, 9=100%

	for(i = 0; i < 10; i++)
		g_work.prog_cnttbl[i] = max * (i + 1) / 10;
}


//=======================
// main
//=======================


/* イメージ出力
 *
 * return: 0 以外でエラー */

static int _output_image(mImageBuf2 *img,mStr *strfname)
{
	mStr str = MSTR_INIT;
	const char *ext[] = {
		"png","jpg","bmp","tiff","webp","tga","psd","avif"
	};
	int ret;

	//出力ファイル名

	if(mStrIsnotEmpty(&g_opt.strOutputName))
	{
		//--output でファイル名指定

		mStrCopy(&str, &g_opt.strOutputName);
	}
	else if(mStrIsnotEmpty(&g_opt.strOutputDir))
	{
		//--dir でディレクトリ指定

		mStrPathGetOutputFile(&str, strfname->buf,
			g_opt.strOutputDir.buf, ext[g_opt.out_format - 1]);
	}
	else
	{
		//指定なし: "[name]_r.[ext]"

		mStrPathGetBasename_noext(&str, strfname->buf);

		mStrAppendText(&str, "_r.");
		mStrAppendText(&str, ext[g_opt.out_format - 1]);
	}

	//出力

	ret = image_save(img, str.buf);

	mStrFree(&str);

	return ret;
}

/* ファイル処理 */

static void _proc_file(mStr *strfname)
{
	mImageBuf2 *img,*imgdst;
	mSize ssize,dsize;
	int ret,is_resize,is_crop,is_alpha,n;

	//画像読み込み
	//(エラーは関数内で出力される)

	img = image_load(strfname->buf);
	if(!img) return;

	//サイズ取得

	if(_get_imgsize(img, &ssize, &dsize))
	{
		mImageBuf2_free(img);
		return;
	}

	//リサイズ、切り取りがあるか

	is_alpha = is_output_alpha();
	is_resize = (ssize.w != dsize.w || ssize.h != dsize.h);
	is_crop = (img->width != ssize.w || img->height != ssize.h);

	//リサイズがある or 解像度なしの場合、解像度は出力しない

	if(is_resize || (g_opt.flags & OPTION_F_NO_RESO))
		g_work.dpi_x = g_work.dpi_y = 0;

	//解像度固定

	if(g_opt.fix_reso)
		g_work.dpi_x = g_work.dpi_y = g_opt.fix_reso;

	//情報

	if(g_opt.putmes)
	{
		//サイズ

		printf("(%dx%d)", img->width, img->height);
		
		if(is_crop)
		{
			printf(" -> crop (%d,%d,%dx%d)",
				g_opt.rcCrop.x1, g_opt.rcCrop.y1, ssize.w, ssize.h);
		}

		if(is_resize)
			printf(" -> (%dx%d)", dsize.w, dsize.h);

		printf(": ");

		//解像度

		if(g_work.dpi_x)
			printf("%ddpi: ", g_work.dpi_x);

		fflush(stdout);
	}

	//経過表示開始
	// :各処理ごとに、進捗値のバランスを取るため、画像の高さを基準とする。
	// :アルファ出力時、ぼかしは無効。

	n = dsize.h;  //画像保存時用、px 換算
	if(is_resize) n += ssize.h + dsize.h;
	if(!is_alpha && g_opt.blur_type) n += img->height;

	_start_progress(n);

	//リサイズ前のイメージ処理 (アルファ出力時無効)
	// :ぼかしは経過対象

	if(!is_alpha)
		image_before_proc(img);

	//出力イメージ

	if(!is_resize)
	{
		//--- リサイズなし

		imgdst = img;

		//切り取り

		if(is_crop)
		{
			if(!mImageBuf2_crop_keep(img,
				g_opt.rcCrop.x1, g_opt.rcCrop.y1, ssize.w, ssize.h, 0))
			{
				putmes("alloc error\n");
				mImageBuf2_free(img);
				return;
			}
		}
	}
	else
	{
		//--- リサイズ
		// :切り取りも同時に行う。img は常に解放される

		imgdst = image_resize(img, ssize.w, ssize.h, dsize.w, dsize.h);
		if(!imgdst)
		{
			putmes("alloc error\n");
			return;
		}
	}

	//リサイズ後のイメージ処理

	image_after_proc(imgdst);

	//画像保存

	ret = _output_image(imgdst, strfname);

	mImageBuf2_free(imgdst);

	//

	if(ret)
		putmes(" save error\n");
	else
		putmes(" ok\n");
}

/* 初期化 */

static void _init(void)
{
	//オプションデータ

	mMemset0(&g_opt, sizeof(OptionData));

	g_opt.resize_type = RESIZE_TYPE_LANCZOS2;
	g_opt.putmes = 1;
	g_opt.bkgndcol = 0xffffff;

	//作業用データ

	mMemset0(&g_work, sizeof(WorkData));
}

/** main */

int main(int argc,char **argv)
{
	int i;
	mStr str = MSTR_INIT;

	mInitLocale();
	
	_init();

	i = main_get_options(argc, argv);

	//

	for(; i < argc; i++)
	{
		//ロケール文字列のまま出力
		putmes("%s: ", argv[i]);

		//UTF-8 文字列
		mStrSetText_locale(&str, argv[i], -1);

		_proc_file(&str);
	}

	mStrFree(&str);

	//

	exit_app(0);

	return 0;
}


//=======================
// 共通関数
//=======================


/** アルファ付きで出力するか
 *
 * JPEG 出力の場合は、常に OFF */

int is_output_alpha(void)
{
	return ((g_opt.flags & OPTION_F_ALPHA)
		&& g_opt.out_format != FORMAT_JPEG);
}

/** アプリ終了
 *
 * ret: 戻り値 */

void exit_app(int ret)
{
	mStrFree(&g_opt.strOutputDir);
	mStrFree(&g_opt.strOutputName);

	ResizeParam_free(&g_work.resizex);
	ResizeParam_free(&g_work.resizey);

	exit(ret);
}

/** エラーを表示して、エラー終了させる
 *
 * 終端には改行が出力される。 */

void puterr_exit(const char *fmt,...)
{
	va_list ap;

	if(g_opt.putmes)
	{
		va_start(ap, fmt);
		vfprintf(stderr, fmt, ap);
		fputc('\n', stderr);
		va_end(ap);
	}

	exit_app(1);
}

/** 通常メッセージ表示
 *
 * 終端に改行は出力されない。 */

void putmes(const char *fmt,...)
{
	va_list ap;

	if(g_opt.putmes)
	{
		va_start(ap, fmt);
		vprintf(fmt, ap);
		va_end(ap);

		fflush(stdout);
	}
}

/** 経過カウンタを加算
 *
 * val: -1 でインクリメント。
 *    0 以上で prog_offset の位置からの相対値 */

void progress_add(int val)
{
	int i,cnt;

	if(!g_opt.putmes) return;

	if(val == -1)
		g_work.prog_curcnt++;
	else
		g_work.prog_curcnt = g_work.prog_offset + val;

	//

	cnt = g_work.prog_curcnt;

	for(i = g_work.prog_curmark; i < 10; i++)
	{
		if(cnt < g_work.prog_cnttbl[i]) break;

		putchar('*');
	}

	if(i != g_work.prog_curmark)
	{
		g_work.prog_curmark = i;

		fflush(stdout);
	}
}
