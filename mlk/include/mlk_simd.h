/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_SIMD_H
#define MLK_SIMD_H

#define MLK_ENABLE_SSE    0
#define MLK_ENABLE_SSE2   0
#define MLK_ENABLE_SSE3   0
#define MLK_ENABLE_SSSE3  0
#define MLK_ENABLE_SSE4_1 0
#define MLK_ENABLE_SSE4_2 0
#define MLK_ENABLE_AVX    0
#define MLK_ENABLE_AVX2   0
#define MLK_ENABLE_FMA    0


#if !defined(MLK_NO_SIMD)

#ifdef HAVE_SSE
# include "xmmintrin.h"
# undef  MLK_ENABLE_SSE
# define MLK_ENABLE_SSE 1
#endif

#ifdef HAVE_SSE2
# include "emmintrin.h"
# undef  MLK_ENABLE_SSE2
# define MLK_ENABLE_SSE2 1
#endif

#ifdef HAVE_SSE3
# include "pmmintrin.h"
# undef  MLK_ENABLE_SSE3
# define MLK_ENABLE_SSE3 1
#endif

#ifdef HAVE_SSSE3
# include "tmmintrin.h"
# undef  MLK_ENABLE_SSSE3
# define MLK_ENABLE_SSSE3 1
#endif

#ifdef HAVE_SSE4_1
# include "smmintrin.h"
# undef  MLK_ENABLE_SSE4_1
# define MLK_ENABLE_SSE4_1 1
#endif

#ifdef HAVE_SSE4_2
# include "nmmintrin.h"
# undef  MLK_ENABLE_SSE4_2
# define MLK_ENABLE_SSE4_2 1
#endif

#ifdef HAVE_AVX
# undef  MLK_ENABLE_AVX
# define MLK_ENABLE_AVX 1
#endif

#ifdef HAVE_AVX2
# undef  MLK_ENABLE_AVX2
# define MLK_ENABLE_AVX2 1
#endif

#ifdef HAVE_FMA
# undef  MLK_ENABLE_FMA
# define MLK_ENABLE_FMA 1
#endif

#if defined(HAVE_AVX) || defined(HAVE_AVX2) || defined(HAVE_FMA)
# include "immintrin.h"
#endif

#endif //NO_SIMD

#endif
