/*$
resizeimg
Copyright (c) 2016-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * イメージ処理
 **********************************/

#include <string.h>

#include <mlk.h>
#include <mlk_imagebuf.h>
#include <mlk_loadimage.h>
#include <mlk_saveimage.h>
#include <mlk_imageconv.h>

#include "def.h"


//==========================
// イメージ処理 (リサイズ前)
//==========================


/* ぼかし (ガウシアン3x3) */

static void _blur_gauss3x3(mImageBuf2 *img)
{
	int w,h,n,ix,iy,i,j,r,g,b,mpos,xpos[3];
	uint8_t **ppbuf,*pdx,*pp,*px;
	uint8_t mt[9] = { 0,1,0, 1,2,1, 0,1,0 }; //シフト値
	int8_t ypos[3];

	ppbuf = img->ppbuf;
	w = img->width;
	h = img->height;

	for(iy = 0; iy < h; iy++, ppbuf++)
	{
		pdx = *ppbuf;

		//Y位置テーブル (相対位置)

		for(i = -1; i <= 1; i++)
		{
			n = iy + i;
			if(n < 0 || n >= h) n = 0;
			else n = i;

			ypos[i + 1] = n;
		}

		//
	
		for(ix = 0; ix < w; ix++, pdx += 4)
		{
			//X位置テーブル (バッファ位置)

			for(j = 0; j < 3; j++)
			{
				n = ix + j - 1;
				if(n < 0) n = 0;
				else if(n >= w) n = w - 1;

				xpos[j] = n << 2;
			}

			//3x3
			
			r = g = b = 0;
			mpos = 0;
			
			for(i = 0; i < 3; i++)
			{
				pp = *(ppbuf + ypos[i]);
				
				for(j = 0; j < 3; j++, mpos++)
				{
					px = pp + xpos[j];
					n = mt[mpos];
					
					r += px[0] << n;
					g += px[1] << n;
					b += px[2] << n;
				}
			}

			r >>= 4;
			g >>= 4;
			b >>= 4;

			if(r > 255) r = 255;
			if(g > 255) g = 255;
			if(b > 255) b = 255;

			//

			pdx[0] = r;
			pdx[1] = g;
			pdx[2] = b;
		}

		progress_add(-1);
	}
}

/* ぼかし (ガウシアン5x5) */

static void _blur_gauss5x5(mImageBuf2 *img)
{
	int w,h,n,ix,iy,i,j,r,g,b,mpos,xpos[5];
	uint8_t **ppbuf,**ppy,*pdx,*pp,*px,*ybuf[5];
	uint8_t mt[25] = {
		1,4,6,4,1, 4,16,24,16,4, 6,24,36,24,6,
		4,16,24,16,4, 1,4,6,4,1
	};
	
	ppbuf = ppy = img->ppbuf;
	w = img->width;
	h = img->height;

	for(iy = 0; iy < h; iy++, ppy++)
	{
		pdx = *ppy;

		//Y 位置テーブル (ポインタ)

		for(i = -2; i <= 2; i++)
		{
			n = iy + i;
			if(n < 0) pp = ppbuf[0];
			else if(n >= h) pp = ppbuf[h - 1];
			else pp = ppy[i];

			ybuf[i + 2] = pp;
		}

		//
	
		for(ix = 0; ix < w; ix++, pdx += 4)
		{
			//X 位置テーブル (バッファ位置)

			for(j = 0; j < 5; j++)
			{
				n = ix + j - 2;
				if(n < 0) n = 0;
				else if(n >= w) n = w - 1;

				xpos[j] = n << 2;
			}
		
			//5x5
			
			r = g = b = 0;
			mpos = 0;
			
			for(i = 0; i < 5; i++)
			{
				pp = ybuf[i];
				
				for(j = 0; j < 5; j++, mpos++)
				{
					px = pp + xpos[j];
					n = mt[mpos];

					r += px[0] * n;
					g += px[1] * n;
					b += px[2] * n;
				}
			}

			r >>= 8;
			g >>= 8;
			b >>= 8;

			if(r > 255) r = 255;
			if(g > 255) g = 255;
			if(b > 255) b = 255;

			//

			pdx[0] = r;
			pdx[1] = g;
			pdx[2] = b;
		}

		progress_add(-1);
	}
}

/* アルファ合成
 *
 * RGBA -> RGBX にする */

static void _blend_alpha(mImageBuf2 *img)
{
	uint8_t **pp,*pd;
	int ix,iy,a,dr,dg,db;

	a = g_opt.bkgndcol;
	dr = MLK_RGB_R(a);
	dg = MLK_RGB_G(a);
	db = MLK_RGB_B(a);

	pp = img->ppbuf;

	for(iy = img->height; iy; iy--)
	{
		pd = *(pp++);
	
		for(ix = img->width; ix; ix--, pd += 4)
		{
			a = pd[3];
		
			if(a == 0)
			{
				pd[0] = dr;
				pd[1] = dg;
				pd[2] = db;
			}
			else if(a != 255)
			{
				pd[0] = (pd[0] - dr) * a / 255 + dr;
				pd[1] = (pd[1] - dg) * a / 255 + dg;
				pd[2] = (pd[2] - db) * a / 255 + db;
			}
		}
	}
}

/* リサイズ前のイメージ処理
 *
 * アルファ合成、ぼかし */

void image_before_proc(mImageBuf2 *img)
{
	//アルファ合成

	_blend_alpha(img);

	//ぼかし (進捗対象)

	if(g_opt.blur_type)
	{
		if(g_opt.blur_type == BLUR_TYPE_GAUSS3x3)
			_blur_gauss3x3(img);
		else if(g_opt.blur_type == BLUR_TYPE_GAUSS5x5)
			_blur_gauss5x5(img);
	}
}


//==========================
// イメージ処理 (リサイズ後)
//==========================


/* リサイズ後のイメージ処理
 *
 * - グレイスケール/2階調化
 * - RGB に値追加 */

void image_after_proc(mImageBuf2 *img)
{
	uint8_t **pp,*pd;
	int ix,iy,r,g,b,addr,addg,addb;
	uint8_t fgrayscale,fadd,fmono;

	fgrayscale = ((g_opt.flags & OPTION_F_GRAYSCALE) != 0);
	fmono = ((g_opt.flags & OPTION_F_MONOCHROME) != 0);

	if(fmono) fgrayscale = TRUE;

	addr = g_opt.addcol[0];
	addg = g_opt.addcol[1];
	addb = g_opt.addcol[2];

	fadd = (addr || addg || addb);

	if(!fgrayscale && !fmono && !fadd) return;

	//

	pp = img->ppbuf;

	for(iy = img->height; iy; iy--)
	{
		pd = *(pp++);
	
		for(ix = img->width; ix; ix--, pd += 4)
		{
			r = pd[0], g = pd[1], b = pd[2];

			//色追加

			if(fadd)
			{
				r += addr;
				g += addg;
				b += addb;

				if(r < 0) r = 0; else if(r > 255) r = 255;
				if(g < 0) g = 0; else if(g > 255) g = 255;
				if(b < 0) b = 0; else if(b > 255) b = 255;
			}

			//グレイスケール化

			if(fgrayscale && !(r == g && r == b))
				r = g = b = (r * 77 + g * 150 + b * 29) >> 8;

			//2階調化

			if(fmono && r > 0 && r < 255)
				r = g = b = (r < 128)? 0: 255;

			pd[0] = r, pd[1] = g, pd[2] = b;
		}
	}
}


//==========================
// 画像ファイル読み込み
//==========================


static mFuncLoadImageCheck g_check_funcs[] = {
	mLoadImage_checkPNG, mLoadImage_checkJPEG, mLoadImage_checkBMP,
	mLoadImage_checkGIF, mLoadImage_checkPSD,
	
#if defined(HAVE_LIB_WEBP)
	mLoadImage_checkWEBP,
#endif

#if defined(HAVE_LIB_TIFF)
	mLoadImage_checkTIFF,
#endif

#if defined(HAVE_LIB_AVIF)
	mLoadImage_checkAVIF,
#endif

	mLoadImage_checkTGA, 0 //TGA は最後にチェック
};


/** 画像ファイル読み込み */

mImageBuf2 *image_load(const char *filename)
{
	mLoadImage li;
	mLoadImageType type;
	mImageBuf2 *img = NULL;
	int err,h,v;
	const char *errmes = "error\n";

	mLoadImage_init(&li);

	li.open.type = MLOADIMAGE_OPEN_FILENAME;
	li.open.filename = filename;
	li.convert_type = MLOADIMAGE_CONVERT_TYPE_RGBA;
	li.flags = MLOADIMAGE_FLAGS_TRANSPARENT_TO_ALPHA;

	//ヘッダからタイプ判定

	err = mLoadImage_checkFormat(&type, &li.open, g_check_funcs, 0);
	if(err)
	{
		if(err == MLKERR_UNSUPPORTED)
			putmes("unsupported format\n");
		else
			putmes(errmes);

		return NULL;
	}

	//開く

	if((type.open)(&li)) goto ERR;

	//最大サイズ

	if(li.width > IMAGESIZE_MAX || li.height > IMAGESIZE_MAX)
	{
		errmes = "image size is too large\n";
		goto ERR;
	}

	//画像確保

	img = mImageBuf2_new(li.width, li.height, 32, 0);
	if(!img) goto ERR;

	//解像度

	if(!mLoadImage_getDPI(&li, &h, &v))
		h = v = 0;

	g_work.dpi_x = h;
	g_work.dpi_y = v;

	//イメージ読み込み

	li.imgbuf = img->ppbuf;

	if((type.getimage)(&li))
	{
		errmes = "decode error\n";
		goto ERR;
	}

	//

	(type.close)(&li);

	return img;

	//エラー
ERR:
	(type.close)(&li);
	mImageBuf2_free(img);
	putmes(errmes);
	return NULL;
}


//==========================
// 画像ファイル書き込み
//==========================


/* Y1行イメージセット */

static mlkerr _save_setrow(mSaveImage *p,int y,uint8_t *buf,int line_bytes)
{
	uint8_t *ps;

	ps = *((uint8_t **)p->param1 + y);

	switch(p->coltype)
	{
		//RGB/A
		case MSAVEIMAGE_COLTYPE_RGB:
			if(p->samples_per_pixel == 3)
				mImageConv_rgbx8_to_rgb8(buf, ps, p->width);
			else
				memcpy(buf, ps, line_bytes);
			break;

		//GRAYSCALE (1/8bit)
		case MSAVEIMAGE_COLTYPE_GRAY:
			if(p->bits_per_sample == 8)
				mImageConv_rgbx8_to_gray8(buf, ps, p->width);
			else
				mImageConv_rgbx8_to_gray1(buf, ps, p->width);
			break;
	}

	return MLKERR_OK;
}

/* Y1行イメージセット (PSD) */

static mlkerr _save_setrow_ch(mSaveImage *p,int y,int ch,uint8_t *buf,int line_bytes)
{
	uint8_t *ps;
	int i;

	ps = *((uint8_t **)p->param1 + y) + ch;

	if(p->coltype == MSAVEIMAGE_COLTYPE_GRAY
		&& p->bits_per_sample == 1)
	{
		//GRAY 1bit

		mImageConv_rgbx8_to_gray1(buf, ps, p->width);
	}
	else
	{
		//GRAY 8bit or RGB/A
	
		for(i = p->width; i; i--, ps += 4)
			*(buf++) = *ps;
	}

	return MLKERR_OK;
}

/* 進捗 */

static void _save_progress(mSaveImage *p,int percent)
{
	//100% = 画像高さとして換算する。
	//prog_offset からの相対値

	progress_add(p->height * percent / 100);
}


//-------------------

//OptionData::opt_format に対応した配列

typedef struct
{
	mFuncSaveImage func;
	int flags; //グレイスケール/1bitMONO 出力対応フラグ (bit0:gray, bit1:mono)
}_savefmt;

#define SAVEFMT_F_GRAY  1
#define SAVEFMT_F_MONO  2
#define SAVEFMT_F_GRAY_MONO 3

static const _savefmt g_saveformat[] = {
	{mSaveImagePNG, SAVEFMT_F_GRAY_MONO},
	{mSaveImageJPEG, SAVEFMT_F_GRAY},
	{mSaveImageBMP, 0},

#if defined(HAVE_LIB_TIFF)
	{mSaveImageTIFF, SAVEFMT_F_GRAY_MONO},
#else
	{0, 0},
#endif

#if defined(HAVE_LIB_WEBP)
	{mSaveImageWEBP, 0},
#else
	{0, 0},
#endif

	{mSaveImageTGA, SAVEFMT_F_GRAY},
	{mSaveImagePSD, SAVEFMT_F_GRAY_MONO},

#if defined(HAVE_LIB_AVIF)
	{mSaveImageAVIF, SAVEFMT_F_GRAY}
#else
	{0,0}
#endif
};

//-------------------


/** 画像ファイル書き込み
 *
 * return: 0 以外でエラー */

int image_save(mImageBuf2 *img,const char *filename)
{
	mSaveImage si;
	mSaveImageOpt opt;
	const _savefmt *sf;
	uint16_t tiffcomp[] = {
		MSAVEOPT_TIFF_COMPRESSION_NONE, MSAVEOPT_TIFF_COMPRESSION_LZW,
		MSAVEOPT_TIFF_COMPRESSION_DEFLATE, MSAVEOPT_TIFF_COMPRESSION_PACKBITS
	};
	int format,f;

	format = g_opt.out_format;

	sf = g_saveformat + (format - 1);

	//

	mSaveImage_init(&si);

	si.open.type = MSAVEIMAGE_OPEN_FILENAME;
	si.open.filename = filename;
	si.width = img->width;
	si.height = img->height;
	si.bits_per_sample = 8;
	si.setrow = _save_setrow;
	si.setrow_ch = _save_setrow_ch;
	si.progress = _save_progress;
	si.param1 = img->ppbuf;

	//解像度

	if(g_work.dpi_x)
	{
		si.reso_unit = MSAVEIMAGE_RESOUNIT_DPI;
		si.reso_horz = g_work.dpi_x;
		si.reso_vert = g_work.dpi_y;
	}

	//カラータイプ

	f = sf->flags;

	if(is_output_alpha())
	{
		//アルファ付き (RGBA)

		si.coltype = MSAVEIMAGE_COLTYPE_RGB;
		si.samples_per_pixel = 4;
	}
	else if((g_opt.flags & OPTION_F_MONOCHROME) && (f & SAVEFMT_F_MONO))
	{
		//GRAY 1bit

		si.coltype = MSAVEIMAGE_COLTYPE_GRAY;
		si.bits_per_sample = 1;
		si.samples_per_pixel = 1;
	}
	else if((g_opt.flags & (OPTION_F_GRAYSCALE | OPTION_F_MONOCHROME))
		&& (f & SAVEFMT_F_GRAY))
	{
		//GRAY 8bit

		si.coltype = MSAVEIMAGE_COLTYPE_GRAY;
		si.samples_per_pixel = 1;
	}
	else
	{
		//RGB

		si.coltype = MSAVEIMAGE_COLTYPE_RGB;
		si.samples_per_pixel = 3;
	}

	//オプション

	opt.png.mask = 0;

	switch(format)
	{
		//PNG
		case FORMAT_PNG:
			opt.png.mask = MSAVEOPT_PNG_MASK_COMP_LEVEL;
			opt.png.comp_level = g_opt.fopt.png.level;
			break;
		//JPEG
		case FORMAT_JPEG:
			opt.jpg.mask = MSAVEOPT_JPEG_MASK_QUALITY
				| MSAVEOPT_JPEG_MASK_PROGRESSION
				| MSAVEOPT_JPEG_MASK_SAMPLING_FACTOR;

			opt.jpg.quality = g_opt.fopt.jpg.quality;
			opt.jpg.progression = g_opt.fopt.jpg.progressive;
			opt.jpg.sampling_factor = g_opt.fopt.jpg.sampling;
			break;
		//TIFF
		case FORMAT_TIFF:
			opt.tiff.mask = MSAVEOPT_TIFF_MASK_COMPRESSION;
			opt.tiff.compression = tiffcomp[g_opt.fopt.tiff.comptype];
			break;
		//WebP
		case FORMAT_WEBP:
			opt.webp.mask = MSAVEOPT_WEBP_MASK_LOSSY
				| MSAVEOPT_WEBP_MASK_LEVEL
				| MSAVEOPT_WEBP_MASK_QUALITY
				| MSAVEOPT_WEBP_MASK_PRESET;

			opt.webp.lossy = g_opt.fopt.webp.type;
			opt.webp.level = g_opt.fopt.webp.level;
			opt.webp.quality = g_opt.fopt.webp.quality;
			opt.webp.preset = g_opt.fopt.webp.preset;
			break;
		//PSD
		case FORMAT_PSD:
			opt.psd.mask = MSAVEOPT_PSD_MASK_COMPRESSION;
			opt.psd.compression = g_opt.fopt.psd.comptype;
			break;
		//AVIF
		case FORMAT_AVIF:
			opt.avif.mask = MSAVEOPT_AVIF_MASK_LOSSLESS
				| MSAVEOPT_AVIF_MASK_QUALITY
				| MSAVEOPT_AVIF_MASK_SPEED
				| MSAVEOPT_AVIF_MASK_CHROMA;
			
			opt.avif.lossless = g_opt.fopt.avif.lossless;
			opt.avif.quality = g_opt.fopt.avif.quality;
			opt.avif.speed = g_opt.fopt.avif.speed;
			opt.avif.chroma = g_opt.fopt.avif.chroma;
			break;
	}

	//保存

	g_work.prog_offset = g_work.prog_curcnt;

	return (sf->func)(&si, &opt);
}
