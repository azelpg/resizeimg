/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_GIFDEC_H
#define MLK_GIFDEC_H

typedef struct _mGIFDec mGIFDec;

enum MGIFDEC_FORMAT
{
	MGIFDEC_FORMAT_RAW = 0,
	MGIFDEC_FORMAT_RGB,
	MGIFDEC_FORMAT_RGBA
};


#ifdef __cplusplus
extern "C" {
#endif

mGIFDec *mGIFDec_new(void);
void mGIFDec_close(mGIFDec *p);

mlkbool mGIFDec_openFile(mGIFDec *p,const char *filename);
mlkbool mGIFDec_openBuf(mGIFDec *p,const void *buf,mlksize bufsize);
mlkbool mGIFDec_openFILEptr(mGIFDec *p,void *fp);

mlkerr mGIFDec_readHeader(mGIFDec *p);

void mGIFDec_getImageSize(mGIFDec *p,mSize *size);
uint8_t *mGIFDec_getPalette(mGIFDec *p,int *pnum);
int mGIFDec_getTransIndex(mGIFDec *p);
int32_t mGIFDec_getTransRGB(mGIFDec *p);

mlkerr mGIFDec_moveNextImage(mGIFDec *p);
mlkerr mGIFDec_getImage(mGIFDec *p);
mlkbool mGIFDec_getNextLine(mGIFDec *p,void *buf,int format,mlkbool trans_to_a0);

#ifdef __cplusplus
}
#endif

#endif
