/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * AVIF 保存
 *****************************************/

#include <stdio.h>
#include <libheif/heif.h>

#include <mlk.h>
#include <mlk_saveimage.h>

//----------------

typedef struct
{
	struct heif_error herr;
	struct heif_context *ctx;
	struct heif_encoder *enc;
	struct heif_image *img;
	struct heif_image_handle *imgh;
}avifdata;

//-----------------


/* エラー判定 */

static int _herror(avifdata *p)
{
	return p->herr.code;
}

/* 書き込み関数 */

static struct heif_error _writer_write(struct heif_context *ctx,
	const void *data,size_t size,void *user)
{
	struct heif_error ret;

	if(fwrite(data, 1, size, (FILE *)user) != size)
	{
		ret.code = heif_error_Encoding_error;
		ret.subcode = heif_suberror_Cannot_write_output_data;
		ret.message = "write error";
	}
	else
	{
		ret.code = 0;
		ret.subcode = 0;
		ret.message = "";
	}

	return ret;
}

/* 圧縮設定をセット */

static void _set_option(struct heif_encoder *enc,mSaveOptAVIF *opt)
{
	int lossless,quality,speed,chroma;
	char m[8];

	lossless = 0;
	quality = 60;
	speed = -1;
	chroma = 420;
	
	if(opt)
	{
		if(opt->mask & MSAVEOPT_AVIF_MASK_LOSSLESS)
			lossless = opt->lossless;

		if(opt->mask & MSAVEOPT_AVIF_MASK_QUALITY)
			quality = opt->quality;

		if(opt->mask & MSAVEOPT_AVIF_MASK_SPEED)
			speed = opt->speed;

		if(opt->mask & MSAVEOPT_AVIF_MASK_CHROMA)
			chroma = opt->chroma;
	}

	//セット

	heif_encoder_set_lossless(enc, lossless);
	heif_encoder_set_lossy_quality(enc, quality);

	if(speed != -1)
		heif_encoder_set_parameter_integer(enc, "speed", speed);

	snprintf(m, 8, "%d", chroma);
	heif_encoder_set_parameter_string(enc, "chroma", m);

	heif_encoder_set_logging_level(enc, 0);
}

/* イメージの作成とセット */

static mlkerr _create_image(avifdata *p,mSaveImage *si)
{
	struct heif_image *img;
	uint8_t *pd;
	int ret,i,pitchd,pitch,height,last_prog,new_prog;
	mFuncSaveImageProgress progress;
	enum heif_colorspace colsp;
	enum heif_chroma chroma;
	enum heif_channel channel;

	//出力カラー (GRAY or RGB/A)

	if(si->coltype == MSAVEIMAGE_COLTYPE_GRAY)
	{
		colsp = heif_colorspace_monochrome;
		chroma = heif_chroma_monochrome;
		channel = heif_channel_Y;
	}
	else
	{
		colsp = heif_colorspace_RGB;
		chroma = (si->samples_per_pixel == 4)? heif_chroma_interleaved_RGBA: heif_chroma_interleaved_RGB;
		channel = heif_channel_interleaved;
	}

	//作成

	p->herr = heif_image_create(si->width, si->height, colsp, chroma, &img);
	if(_herror(p)) return MLKERR_UNKNOWN;

	p->img = img;

	//プレーンを追加

	p->herr = heif_image_add_plane(img, channel, si->width, si->height, 8);
	if(_herror(p)) return MLKERR_UNKNOWN;

	//変換

	pd = heif_image_get_plane(img, channel, &pitchd);

	height = si->height;
	pitch = si->width * si->samples_per_pixel;
	progress = si->progress;
	last_prog = 0;

	for(i = 0; i < height; i++)
	{
		//取得
	
		ret = (si->setrow)(si, i, pd, pitch);
		if(ret) return ret;

		pd += pitchd;

		//経過

		if(progress)
		{
			new_prog = (i + 1) * 100 / height;

			if(new_prog != last_prog)
			{
				(progress)(si, new_prog);
				last_prog = new_prog;
			}
		}
	}

	return MLKERR_OK;
}

/* メイン処理 */

static mlkerr _main_proc(avifdata *p,mSaveImage *si,mSaveOptAVIF *opt)
{
	FILE *fp;
	int ret;
	struct heif_writer writer;
	mBufSize meta;

	//コンテキスト

	p->ctx = heif_context_alloc();
	if(!p->ctx) return MLKERR_ALLOC;

	//エンコーダ取得

	p->herr = heif_context_get_encoder_for_format(p->ctx, heif_compression_AV1, &p->enc);
	if(_herror(p)) return MLKERR_UNKNOWN;

	//圧縮設定

	_set_option(p->enc, opt);

	//画像作成

	ret = _create_image(p, si);
	if(ret) return ret;

	//エンコード

	p->herr = heif_context_encode_image(p->ctx, p->img, p->enc, 0, &p->imgh);
	if(_herror(p)) return MLKERR_UNKNOWN;

	//EXIF メタデータ

	if(mSaveImage_createEXIF_resolution(si, &meta))
	{
		heif_context_add_exif_metadata(p->ctx, p->imgh, meta.buf, meta.size);

		mFree(meta.buf);
	}

	//書き込み

	writer.writer_api_version = 1;
	writer.write = _writer_write;

	fp = (FILE *)mSaveImage_openFile(si);
	if(!fp) return MLKERR_OPEN;

	p->herr = heif_context_write(p->ctx, &writer, fp);

	mSaveImage_closeFile(si, fp);

	if(_herror(p)) return MLKERR_IO;

	return MLKERR_OK;
}


//========================


/**@ AVIF 保存 */

mlkerr mSaveImageAVIF(mSaveImage *si,void *opt)
{
	avifdata dat;
	int ret;

	mMemset0(&dat, sizeof(avifdata));

	ret = _main_proc(&dat, si, (mSaveOptAVIF *)opt);

	//

	if(dat.imgh) heif_image_handle_release(dat.imgh);
	if(dat.img) heif_image_release(dat.img);
	if(dat.enc) heif_encoder_release(dat.enc);
	if(dat.ctx) heif_context_free(dat.ctx);

	return ret;
}

