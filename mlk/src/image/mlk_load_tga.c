/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * TGA 読み込み
 *****************************************/

#include <string.h>

#include <mlk.h>
#include <mlk_loadimage.h>
#include <mlk_io.h>
#include <mlk_util.h>
#include <mlk_imageconv.h>


//--------------------

typedef struct
{
	mIO *io;
	
	uint8_t *rawbuf,	//Y1行分の生バッファ
		*palbuf,		//パレットバッファ
		*rlebuf,		//RLE:入力バッファ
		*rlebuf_end,	//RLE:入力データの終端位置
		*rlebuf_cur;	//RLE:展開の現在位置

	int width,
		height,
		coltype,
		bits,			//ビット数 (8,15,16,24,32)
		colmap_index,
		colmap_entnum,	//カラーマップエントリの数
		colmap_bits,
		pitch;			//Y1行バイト数

	uint8_t alpha_bits,		//アルファ値のビット数 (0,1,8)
		is_rle,				//RLE 圧縮か
		is_bottomup,		//下→上方向か
		is_rle_end_file;	//RLE デコード時、ファイルの終端まで読み込んだか
}tgadata;

//--------------------

#define RLE_BUFSIZE  4096

enum
{
	_COLTYPE_PAL = 1,
	_COLTYPE_RGB,
	_COLTYPE_GRAY
};

//--------------------


/** 基本情報を読み込み */

static mlkerr _read_info(tgadata *p,mLoadImage *pli)
{
	uint8_t d[18];
	int n;

	//ヘッダ

	if(mIO_readOK(p->io, d, 18))
		return MLKERR_FORMAT_HEADER;

	//画像形式

	n = d[2];

	if(n == 0)
		return MLKERR_FORMAT_HEADER;
	else if((n >= 4 && n <= 8) || n >= 12)
		return MLKERR_UNSUPPORTED;

	p->coltype = n & 7;
	p->is_rle = (n > 8);

	//カラーマップ

	if(d[1])
	{
		n = d[7];

		if(n != 15 && n != 16 && n != 24 && n != 32)
			return MLKERR_INVALID_VALUE;
	
		p->colmap_index = mGetBufLE16(d + 3);
		p->colmap_entnum = mGetBufLE16(d + 5);
		p->colmap_bits = n;
	}

	//パレット時、カラーマップ数のチェック

	if(p->coltype == _COLTYPE_PAL
		&& (p->colmap_entnum == 0 || p->colmap_entnum > 256))
		return MLKERR_INVALID_VALUE;

	//サイズ

	p->width = mGetBufLE16(d + 12);
	p->height = mGetBufLE16(d + 14);

	if(p->width == 0 || p->height == 0)
		return MLKERR_INVALID_VALUE;

	//ビット数

	n = d[16];

	if(n != 8 && n != 15 && n != 16 && n != 24 && n != 32)
		return MLKERR_INVALID_VALUE;

	p->bits = n;
	p->pitch = ((n + 1) >> 3) * p->width;

	//パレット/グレイスケールは 8bit のみ

	if((p->coltype == _COLTYPE_GRAY || p->coltype == _COLTYPE_PAL)
		&& p->bits != 8)
		return MLKERR_INVALID_VALUE;

	//アルファ値のビット数

	p->alpha_bits = d[17] & 15;

	//ボトムアップ

	if(!(d[17] & (1<<5)))
		p->is_bottomup = TRUE;

	//-------------

	//IDフィールドスキップ

	if(mIO_readSkip(p->io, d[0]))
		return MLKERR_DAMAGED;

	//カラーマップをスキップ

	n = (p->colmap_bits + 1) >> 3;

	if(p->coltype == _COLTYPE_PAL)
		//パレット時は、余分な分をスキップ
		n *= p->colmap_index;
	else
		//パレット以外では、全体をスキップ
		n *= p->colmap_index + p->colmap_entnum;

	if(mIO_readSkip(p->io, n))
		return MLKERR_DAMAGED;

	//------- mLoadImage にセット

	pli->width = p->width;
	pli->height = p->height;
	pli->bits_per_sample = 8;

	//元のカラータイプ

	if(p->coltype == _COLTYPE_PAL)
		n = MLOADIMAGE_COLTYPE_PALETTE;
	else if(p->coltype == _COLTYPE_GRAY)
		n = MLOADIMAGE_COLTYPE_GRAY;
	else
	{
		//32bit で alpha_bits が 8 でない場合は、アルファ値無効として、RGB にする
	
		n = ((p->bits == 32 && p->alpha_bits == 8)
				|| (p->bits == 16 && p->alpha_bits == 1))?
			MLOADIMAGE_COLTYPE_RGBA: MLOADIMAGE_COLTYPE_RGB;
	}

	pli->src_coltype = n;

	//カラータイプ

	mLoadImage_setColorType_fromSource(pli);

	return MLKERR_OK;
}

/** パレットを読み込み */

static mlkerr _read_palette(tgadata *p,mLoadImage *pli)
{
	uint8_t *buf,*ps,*pd;
	int i,bytes,a;
	uint8_t r,g,b;

	bytes = (p->colmap_bits + 1) >> 3;

	//バッファ確保

	buf = p->palbuf = (uint8_t *)mMalloc0(256 * 4);
	if(!buf) return MLKERR_ALLOC;

	//読み込み

	if(mIO_readOK(p->io, buf, p->colmap_entnum * bytes))
		return MLKERR_DAMAGED;

	//変換

	ps = buf + 255 * bytes;
	pd = buf + 255 * 4;

	for(i = 256; i; i--)
	{
		if(bytes >= 3)
		{
			//24/32bit

			r = ps[2];
			g = ps[1];
			b = ps[0];
			a = (bytes == 4)? ps[3]: 255;
		}
		else
		{
			//15/16bit

			a = (ps[1] << 8) | ps[0];

			r = (a >> 10) & 31;
			g = (a >> 5) & 31;
			b = a & 31;

			r = (r == 31)? 255: r << 3;
			g = (g == 31)? 255: g << 3;
			b = (b == 31)? 255: b << 3;
			a = 255;
		}

		pd[0] = r;
		pd[1] = g;
		pd[2] = b;
		pd[3] = a;
	
		ps -= bytes;
		pd -= 4;
	}

	//mLoadImage にセット

	return mLoadImage_setPalette(pli, buf, 256 * 4, 256);
}

/** メモリ確保 */

static mlkbool _alloc_buf(tgadata *p)
{
	//Y1行、生データ

	p->rawbuf = (uint8_t *)mMalloc(p->pitch);
	if(!p->rawbuf) return FALSE;

	//RLE 用

	if(p->is_rle)
	{
		p->rlebuf = (uint8_t *)mMalloc(RLE_BUFSIZE);
		if(!p->rlebuf) return FALSE;

		p->rlebuf_end = p->rlebuf_cur = p->rlebuf;
	}

	return TRUE;
}

/** RLE バッファに入力データ読み込み
 *
 * return: エラーコード */

static mlkerr _rle_read_input(tgadata *p)
{
	int remain,read;

	remain = p->rlebuf_end - p->rlebuf_cur;

	//残りを先頭へ

	memcpy(p->rlebuf, p->rlebuf_cur, remain);

	//読み込み
	//(圧縮後のサイズが不明のため、バッファサイズ分読み込み)

	read = mIO_read(p->io, p->rlebuf + remain, RLE_BUFSIZE - remain);

	//読み込んだサイズが少なければファイルの終端とする

	if(read < RLE_BUFSIZE - remain)
		p->is_rle_end_file = TRUE;

	remain += read;
	
	p->rlebuf_end = p->rlebuf + remain;
	p->rlebuf_cur = p->rlebuf;

	return MLKERR_OK;
}

/** Y1行を RLE 展開 */

static int _decompress_rle(tgadata *p)
{
	uint8_t *pd,*ps,b,v1,v2,v3;
	int ret,bytes,len,insize,curx,width,n;
	uint32_t val;

	//入力データは前回のY行から継続

	ps = p->rlebuf_cur;
	pd = p->rawbuf;
	bytes = (p->bits + 1) >> 3;
	width = p->width;
	insize = p->rlebuf_end - p->rlebuf_cur;
	curx = 0;

	while(curx < width)
	{
		//データ読み込み
		//32bit:非連続128個時、最大 513 byte

		if(insize < 1 + 4 * 128 && !p->is_rle_end_file)
		{
			p->rlebuf_cur = ps;
		
			ret = _rle_read_input(p);
			if(ret) return ret;

			ps = p->rlebuf_cur;
			insize = p->rlebuf_end - p->rlebuf_cur;
		}

		//長さ

		if(insize == 0) return MLKERR_DAMAGED;
		
		b = *(ps++);
		insize--;

		len = (b & 127) + 1;

		if(curx + len > width) return MLKERR_DECODE;

		//[!] バイトオーダーは元のままでコピー

		if(b & 0x80)
		{
			//連続データ

			if(insize < bytes) return MLKERR_DAMAGED;

			if(bytes == 1)
			{
				//8bit

				memset(pd, *ps, len);
				pd += len;
			}
			else if(bytes == 2)
			{
				//15/16bit
				
				val = *((uint16_t *)ps);

				for(n = len; n; n--, pd += 2)
					*((uint16_t *)pd) = val;
			}
			else if(bytes == 3)
			{
				//24bit

				v1 = ps[0], v2 = ps[1], v3 = ps[2];
				
				for(n = len; n; n--, pd += 3)
				{
					pd[0] = v1;
					pd[1] = v2;
					pd[2] = v3;
				}
			}
			else
			{
				//32bit
				
				val = *((uint32_t *)ps);
				
				for(n = len; n; n--, pd += 4)
					*((uint32_t *)pd) = val;
			}

			ps += bytes;
			insize -= bytes;
		}
		else
		{
			//非連続データ

			n = len * bytes;

			if(insize < n) return MLKERR_DAMAGED;

			memcpy(pd, ps, n);

			pd += n;
			ps += n;
			insize -= n;
		}

		curx += len;
	}

	p->rlebuf_cur = ps;

	return MLKERR_OK;
}

/** イメージ読み込み */

static int _read_image(tgadata *p,mLoadImage *pli)
{
	uint8_t **ppbuf,*rawbuf;
	mFuncImageConv funcconv;
	mImageConv conv;
	int pitch,i,ret,height,prog,prog_cur;

#if defined(MLK_BIG_ENDIAN)
	int bit16 = (p->bits == 15 || p->bits == 16);
#endif

	ppbuf = pli->imgbuf;
	rawbuf = p->rawbuf;
	pitch = p->pitch;
	height = p->height;

	if(p->is_bottomup)
		ppbuf += height - 1;
	
	//変換パラメータ

	mLoadImage_setImageConv(pli, &conv);

	conv.srcbuf = rawbuf;
	conv.flags = MIMAGECONV_FLAGS_SRC_BGRA;

	//変換関数

	if(p->coltype == _COLTYPE_GRAY)
		funcconv = mImageConv_gray_1_2_4_8;
	else if(p->coltype == _COLTYPE_PAL)
		funcconv = mImageConv_palette_1_2_4_8;
	else if(p->bits == 24)
		funcconv = mImageConv_rgb8;
	else if(p->bits == 32)
	{
		funcconv = mImageConv_rgba8;

		if(p->alpha_bits != 8)
			conv.flags |= MIMAGECONV_FLAGS_INVALID_ALPHA;
	}
	else
	{
		//15/16bit
		
		funcconv = mImageConv_rgb555;

		if(p->bits == 16 && p->alpha_bits == 1)
			conv.flags |= MIMAGECONV_FLAGS_SRC_ALPHA;
	}

	//---------

	prog_cur = 0;

	for(i = 0; i < height; i++)
	{
		//rawbuf に生データ読み込み

		if(p->is_rle)
		{
			//RLE 圧縮

			ret = _decompress_rle(p);
			if(ret) return ret;
		}
		else
		{
			//無圧縮

			if(mIO_readOK(p->io, rawbuf, pitch))
				return MLKERR_DAMAGED;
		}

		//16bit バイト入れ替え (BE 時)

	#if defined(MLK_BIG_ENDIAN)
		if(bit16)
			mReverseByte_16bit(rawbuf, pitch >> 1);
	#endif

		//変換

		conv.dstbuf = *ppbuf;
		(funcconv)(&conv);

		//

		if(p->is_bottomup)
			ppbuf--;
		else
			ppbuf++;

		//進捗

		if(pli->progress)
		{
			prog = (i + 1) * 100 / height;

			if(prog != prog_cur)
			{
				prog_cur = prog;
				(pli->progress)(pli, prog);
			}
		}
	}

	return MLKERR_OK;
}


//=================================
// main
//=================================


/* 終了 */

static void _tga_close(mLoadImage *pli)
{
	tgadata *p = (tgadata *)pli->handle;

	if(p)
	{
		mIO_close(p->io);

		mFree(p->palbuf);
		mFree(p->rawbuf);
		mFree(p->rlebuf);
	}

	mLoadImage_closeHandle(pli);
}

/* 開く */

static mlkerr _tga_open(mLoadImage *pli)
{
	int ret;

	//作成・開く

	ret = mLoadImage_createHandle(pli, sizeof(tgadata), MIO_ENDIAN_LITTLE);
	if(ret) return ret;

	//基本情報読み込み

	return _read_info((tgadata *)pli->handle, pli);
}

/* イメージ読み込み */

static mlkerr _tga_getimage(mLoadImage *pli)
{
	tgadata *p = (tgadata *)pli->handle;
	int ret;

	//パレット読み込み

	if(p->coltype == _COLTYPE_PAL)
	{
		ret = _read_palette(p, pli);
		if(ret) return ret;
	}

	//メモリ確保

	if(!_alloc_buf(p))
		return MLKERR_ALLOC;

	//読み込み

	return _read_image(p, pli);
}

/* ヘッダ判定 */

static int _check_header(uint8_t type,uint8_t bits)
{
	return ( ((type >= 1 && type <= 3) || (type >= 9 && type <= 11))
		&& (bits == 8 || bits == 15 || bits == 16 || bits == 24 || bits == 32) );
}

/**@ TGA 判定と関数セット */

mlkbool mLoadImage_checkTGA(mLoadImageType *p,uint8_t *buf,int size)
{
	if(!buf || (size >= 17 && _check_header(buf[2], buf[16])) )
	{
		p->format_tag = MLK_MAKE32_4('T','G','A',' ');
		p->open = _tga_open;
		p->getimage = _tga_getimage;
		p->close = _tga_close;
		
		return TRUE;
	}

	return FALSE;
}
