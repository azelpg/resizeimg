/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_RECTBOX_H
#define MLK_RECTBOX_H

#ifdef __cplusplus
extern "C" {
#endif

mlkbool mRectIsEmpty(const mRect *rc);
void mRectEmpty(mRect *rc);
void mRectSet(mRect *rc,int x1,int y1,int x2,int y2);
void mRectSetPack(mRect *rc,uint32_t val);
void mRectSetBox_d(mRect *rc,int x,int y,int w,int h);
void mRectSetBox(mRect *rc,const mBox *box);
void mRectSetPoint(mRect *rc,const mPoint *pt);
void mRectSetPoint_minmax(mRect *rc,const mPoint *pt1,const mPoint *pt2);
void mRectSwap_minmax(mRect *rc);
void mRectSwap_minmax_to(mRect *dst,const mRect *src);
void mRectUnion(mRect *dst,const mRect *src);
void mRectUnion_box(mRect *dst,const mBox *box);
mlkbool mRectClipBox_d(mRect *rc,int x,int y,int w,int h);
mlkbool mRectClipRect(mRect *rc,const mRect *clip);
mlkbool mRectIsInRect(mRect *rc,const mRect *rcin);
mlkbool mRectIsPointIn(const mRect *rc,int x,int y);
mlkbool mRectIsCross(const mRect *rc1,const mRect *rc2);
void mRectIncPoint(mRect *rc,int x,int y);
void mRectDeflate(mRect *rc,int dx,int dy);
void mRectMove(mRect *rc,int mx,int my);

void mBoxSet(mBox *box,int x,int y,int w,int h);
void mBoxSetPoint_minmax(mBox *box,const mPoint *pt1,const mPoint *pt2);
void mBoxSetRect(mBox *box,const mRect *rc);
void mBoxSetRect_empty(mBox *box,const mRect *rc);
void mBoxSetPoints(mBox *box,const mPoint *pt,int num);
void mBoxUnion(mBox *dst,const mBox *src);
void mBoxResize_keepaspect(mBox *box,int boxw,int boxh,mlkbool noup);
mlkbool mBoxIsCross(const mBox *box1,const mBox *box2);
mlkbool mBoxIsPointIn(const mBox *box,int x,int y);

#ifdef __cplusplus
}
#endif

#endif
