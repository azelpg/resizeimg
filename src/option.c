/*$
resizeimg
Copyright (c) 2016-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * オプション処理
 **********************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_string.h>
#include <mlk_file.h>
#include <mlk_argparse.h>

#include "def.h"


//--------------

#define VERSION_TEXT  "resizeimg ver 2.0.5\nCopyright (c) 2016-2023 Azel"

//--fopt オプションの値の最大数
#define _FORMATOPT_MAX  4

//--fopt の各値

typedef struct
{
	char type;	//値の名前 (1文字)
	int len;	//値の文字長さ
	const char *str; //値の先頭位置
}_formatopt;

//引数処理用構造体

typedef struct
{
	mArgParse ap;
	_formatopt fopt[_FORMATOPT_MAX + 1]; //--fopt の値
}_argparse;

//--------------



/* 引数から数値取得
 *
 * 指定範囲外の値ならエラー終了 */

static int _getopt_val(char *arg,int min,int max,const char *optname)
{
	int n;

	n = atoi(arg);

	if(n >= min && n <= max)
		return n;
	else
	{
		puterr_exit("--%s: invalid value", optname);
		return -1;
	}
}

/* キーワードとなる文字列からインデックス取得
 *
 * name : 検索する名前
 * longlist : 長い形式の文字列 (; で区切る)
 * shortlist : 短い形式の文字列。NULL でなし
 * optname : 長い名前のオプション名 (エラー表示時用) */

static int _getopt_keyword(char *name,const char *longlist,const char *shortlist,const char *optname)
{
	int ind;

	ind = mStringGetSplitTextIndex(name, -1, longlist, ';', FALSE);
	if(ind != -1) return ind;

	if(shortlist)
	{
		ind = mStringGetSplitTextIndex(name, -1, shortlist, ';', FALSE);
		if(ind != -1) return ind;
	}

	puterr_exit("--%s: '%s' is invalid", optname, name);

	return -1;
}

/* 出力ファイル名の拡張子から、フォーマットを自動判別 */

static void _set_outformat(void)
{
	mStr ext = MSTR_INIT;
	char *name;
	int n;

	mStrPathGetExt(&ext, g_opt.strOutputName.buf);
	name = ext.buf;

	if(strcasecmp(name, "bmp") == 0)
		n = FORMAT_BMP;
	else if(strcasecmp(name, "png") == 0)
		n = FORMAT_PNG;
	else if(strcasecmp(name, "jpg") == 0
		|| strcasecmp(name, "jpeg") == 0)
		n = FORMAT_JPEG;
	else if(strcasecmp(name, "tiff") == 0
		|| strcasecmp(name, "tif") == 0)
		n = FORMAT_TIFF;
	else if(strcasecmp(name, "webp") == 0)
		n = FORMAT_WEBP;
	else if(strcasecmp(name, "avif") == 0)
		n = FORMAT_AVIF;
	else if(strcasecmp(name, "psd") == 0)
		n = FORMAT_PSD;
	else if(strcasecmp(name, "tga") == 0)
		n = FORMAT_TGA;
	else
		n = FORMAT_PNG;

	g_opt.out_format = n;

	mStrFree(&ext);
}


//==============================
// 出力フォーマットオプション
//==============================

/* type = p = 0 : FormatOpt に初期値をセット。
 * type = -1, p = 0 : オプション値を表示。
 * p != 0 : type に値の名前の1文字。0 を返すと、未知のオプション。 */

typedef int (*formatopt_func)(char type,_formatopt *p);


/* 数値取得 */

static int _formatopt_getval(_formatopt *p,int min,int max)
{
	int n;

	if(!p->str)
	{
		puterr_exit("--fopt: need value: '%c'", p->type);
		return 0;
	}
	else
	{
		n = atoi(p->str);

		if(n < min || n > max)
		{
			puterr_exit("--fopt: invalid value: '%c'", p->type);
			return 0;
		}

		return n;
	}
}

/* 列挙文字列からインデックス取得 */

static int _formatopt_getkeyword(_formatopt *p,const char *keywords)
{
	const char *pc,*end;
	int ind = 0;

	if(!p->str)
	{
		puterr_exit("--fopt: need value: '%c'", p->type);
		return 0;
	}

	pc = keywords;
	
	while(mStringGetNextSplit(&pc, &end, ';'))
	{
		if(strncmp(pc, p->str, p->len) == 0)
			return ind;

		ind++;
		pc = end;
	}

	puterr_exit("--fopt: invalid param: '%*s'", p->len, p->str);
	return -1;
}

/* PNG */

static int _formatopt_png(char type,_formatopt *p)
{
	if(!p)
	{
		if(type == 0)
			g_opt.fopt.png.level = 6;
		else
			putmes("level=%d\n", g_opt.fopt.png.level);
	}
	else
	{
		if(type == 'l')
		{
			g_opt.fopt.png.level = _formatopt_getval(p, 0, 9);
			return 1;
		}
	}

	return 0;
}

/* JPEG */

static int _formatopt_jpg(char type,_formatopt *p)
{
	int n,samp[] = {444,422,420};

	if(!p)
	{
		if(type == 0)
		{
			g_opt.fopt.jpg.quality = 88;
			g_opt.fopt.jpg.sampling = 420;
		}
		else
		{
			putmes("quality=%d, sampling=%d%s\n",
				g_opt.fopt.jpg.quality,
				g_opt.fopt.jpg.sampling,
				(g_opt.fopt.jpg.progressive)? ", progressive": "");
		}
	}
	else
	{
		switch(type)
		{
			case 'q':
				g_opt.fopt.jpg.quality = _formatopt_getval(p, 0, 100);
				return 1;
			case 'p':
				g_opt.fopt.jpg.progressive = 1;
				return 1;
			//サンプリング比
			case 's':
				n = _formatopt_getkeyword(p, "hi;mid;low");
				g_opt.fopt.jpg.sampling = samp[n];
				return 1;
		}
	}

	return 0;
}

/* TIFF */

static int _formatopt_tiff(char type,_formatopt *p)
{
	const char *comp[] = {"none","lzw","deflate","packbits"};

	if(!p)
	{
		if(type == 0)
			g_opt.fopt.tiff.comptype = TIFF_COMP_LZW;
		else
			putmes("compression=%s\n", comp[g_opt.fopt.tiff.comptype]);
	}
	else
	{
		if(type == 'c')
		{
			g_opt.fopt.tiff.comptype =
				_formatopt_getkeyword(p, "none;lzw;deflate;packbits");
			return 1;
		}
	}

	return 0;
}

/* WebP */

static int _formatopt_webp(char type,_formatopt *p)
{
	if(!p)
	{
		if(type == 0)
			g_opt.fopt.webp.level = 6;
		else
		{
			if(g_opt.fopt.webp.type)
				putmes("lossy, quality=%d, preset=%d\n", g_opt.fopt.webp.quality, g_opt.fopt.webp.preset);
			else
				putmes("lossless, level=%d\n", g_opt.fopt.webp.level);
		}
	}
	else
	{
		switch(type)
		{
			case 'l':
				g_opt.fopt.webp.type = 0;
				g_opt.fopt.webp.level = _formatopt_getval(p, 0, 9);
				return 1;
			case 'q':
				g_opt.fopt.webp.type = 1;
				g_opt.fopt.webp.quality = _formatopt_getval(p, 0, 100);
				return 1;
			//プリセット
			case 'p':
				g_opt.fopt.webp.preset = _formatopt_getkeyword(p, "default;picture;photo;drawing;icon;text");
				return 1;
		}
	}

	return 0;
}

/* PSD */

static int _formatopt_psd(char type,_formatopt *p)
{
	const char *comp[] = {"none","packbits"};

	if(!p)
	{
		if(type == 0)
			g_opt.fopt.psd.comptype = 1;
		else
			putmes("compression=%s\n", comp[g_opt.fopt.psd.comptype]);
	}
	else
	{
		if(type == 'c')
		{
			g_opt.fopt.psd.comptype = _formatopt_getval(p, 0, 1);
			return 1;
		}
	}

	return 0;
}

/* AVIF */

static int _formatopt_avif(char type,_formatopt *p)
{
	int n;

	if(!p)
	{
		if(type == 0)
		{
			//デフォルト
			
			g_opt.fopt.avif.lossless = 0;
			g_opt.fopt.avif.quality = 60;
			g_opt.fopt.avif.speed = 5;
			g_opt.fopt.avif.chroma = 420;
		}
		else
		{
			putmes("lossless=%d, quality=%d, speed=%d, chroma=%d\n",
				g_opt.fopt.avif.lossless, g_opt.fopt.avif.quality,
				g_opt.fopt.avif.speed, g_opt.fopt.avif.chroma);
		}
	}
	else
	{
		switch(type)
		{
			case 'l':
				g_opt.fopt.avif.lossless = _formatopt_getval(p, 0, 1);
				return 1;
			case 'q':
				g_opt.fopt.avif.quality = _formatopt_getval(p, 0, 100);
				return 1;
			case 's':
				g_opt.fopt.avif.speed = _formatopt_getval(p, 0, 9);
				return 1;
			case 'c':
				n = atoi(p->str);
				if(n == 420 || n == 422 || n == 444)
					g_opt.fopt.avif.chroma = n;
				return 1;
		}
	}

	return 0;
}

/* フォーマットオプションセットと情報表示 */

static void _set_format_opt(_formatopt *src)
{
	formatopt_func func,
		funcdef[] = {
		_formatopt_png, _formatopt_jpg, NULL,
		_formatopt_tiff, _formatopt_webp, NULL,
		_formatopt_psd, _formatopt_avif
	};
	const char *type[] = {
		"PNG","JPEG","BMP","TIFF","WebP","TGA","PSD","AVIF"
	};

	//出力タイプ

	putmes("# output [%s]: ", type[g_opt.out_format - 1]);

	//

	func = funcdef[g_opt.out_format - 1];

	if(!func)
		putmes("no option\n");
	else
	{
		//デフォルトの値を設定

		(func)(0, NULL);

		//各項目の値をセット

		for(; src->type; src++)
		{
			if(!(func)(src->type, src))
				puterr_exit("--fopt: unknown option: '%c'", src->type);
		}

		//オプションを表示

		(func)(-1, NULL);
	}
}


//=======================
// オプション取得
//=======================


/* --output */

static void _opt_output(mArgParse *p,char *arg)
{
	mStrSetText_locale(&g_opt.strOutputName, arg, -1);
}

/* --dir */

static void _opt_dir(mArgParse *p,char *arg)
{
	mStrSetText_locale(&g_opt.strOutputDir, arg, -1);

	//ディレクトリが存在するか
	
	if(mStrIsnotEmpty(&g_opt.strOutputDir)
		&& !mIsExistDir(g_opt.strOutputDir.buf))
	{
		puterr_exit("--dir: directory does not exist");
	}
}

/* --type
 *
 * デフォルトで -o による自動判別 */

static void _opt_type(mArgParse *p,char *arg)
{
	g_opt.out_format = _getopt_keyword(arg,
		"png;jpeg;bmp;tiff;webp;tga;psd;avif", NULL, "type") + 1;
}

/* --fopt : フォーマットオプション
 *
 * 出力フォーマットによって解釈が異なるため、後で処理する */

static void _opt_fopt(mArgParse *p,char *arg)
{
	_formatopt *fopt;
	const char *pc,*end;
	int len,cnt = 0;

	fopt = ((_argparse *)p)->fopt;
	pc = arg;

	while(mStringGetNextSplit(&pc, &end, ',') && cnt < _FORMATOPT_MAX)
	{
		len = end - pc;

		if(len == 1)
		{
			//値なし

			fopt->type = *pc;
			fopt->str = NULL;

			fopt++;
			cnt++;
		}
		else if(len >= 3 && pc[1] == '=')
		{
			//値あり

			fopt->type = *pc;
			fopt->str = pc + 2;
			fopt->len = len - 2;

			fopt++;
			cnt++;
		}
		else
			puterr_exit("--fopt: invalid value: \"%*s\"", len, pc);

		pc = end;
	}
}

/* --resize : リサイズタイプ */

static void _opt_resize(mArgParse *p,char *arg)
{
	if(*arg >= '0' && *arg <= '9')
		//先頭文字が数字なら、値で取得
		g_opt.resize_type = _getopt_val(arg, 0, RESIZE_TYPE_NUM - 1, "resize");
	else
	{
		//キーワードで取得
		
		g_opt.resize_type = _getopt_keyword(arg,
			"mitchell;lagrange;lanczos2;lanczos3;spline16;spline36;bicubic;blackmansinc2;blackmansinc3",
			"mit;lag;lc2;lc3;s16;s36;bic;bl2;bl3",
			"resize");
	}
}

/* --size : 出力サイズ */

static void _opt_size(mArgParse *p,char *arg)
{
	char *pc;
	int n;

	n = strlen(arg);

	if(n && arg[n - 1] == '%')
	{
		//N%

		g_opt.dscale = atof(arg) / 100.0;
		g_opt.out_width = g_opt.out_height = 0;

		if(g_opt.dscale <= 0)
			puterr_exit("--size: invalid value");
	}
	else
	{
		//WxH
		
		pc = strchr(arg, 'x');
		if(!pc)
			puterr_exit("--size: 'WIDTHxHEIGHT' or 'N%%'");

		g_opt.out_width  = atoi(arg);
		g_opt.out_height = atoi(pc + 1);
		g_opt.dscale = 0;

		if(g_opt.out_width <= 0 || g_opt.out_height <= 0)
			puterr_exit("--size: invalid value");
	}
}

/* --width : 出力幅 */

static void _opt_width(mArgParse *p,char *arg)
{
	g_opt.out_width = atoi(arg);
	g_opt.out_height = 0;
	g_opt.dscale = 0;

	if(g_opt.out_width <= 0)
		puterr_exit("--width: invalid value");
}

/* --height : 出力高さ */

static void _opt_height(mArgParse *p,char *arg)
{
	g_opt.out_width = 0;
	g_opt.out_height = atoi(arg);
	g_opt.dscale = 0;

	if(g_opt.out_height <= 0)
		puterr_exit("--height: invalid value");
}

/* --alpha : アルファ付き */

static void _opt_alpha(mArgParse *p,char *arg)
{
	g_opt.flags |= OPTION_F_ALPHA;
}

/* --bkgnd : 背景色 */

static void _opt_bkgnd(mArgParse *p,char *arg)
{
	g_opt.bkgndcol = strtoul(arg, NULL, 16);
}

/* --crop : 切り取り */

static void _opt_crop(mArgParse *p,char *arg)
{
	const char *pc,*pcend,*tmp;
	int val[4],n,pos = 0;

	mMemset0(val, sizeof(int) * 4);

	pc = arg;

	while(mStringGetNextSplit(&pc, &pcend, ','))
	{
		//3番目の場合、WxH か
		
		if(pos == 2)
		{
			for(tmp = pc; tmp < pcend && *tmp != 'x'; tmp++);

			//tmp = 'x' の位置 or pcend
			if(tmp != pcend)
			{
				val[2] = atoi(pc);
				val[3] = atoi(tmp + 1);

				if(val[2] <= 0 || val[3] <= 0)
					puterr_exit("--crop: invalid value");

				g_opt.flags |= OPTION_F_CROP_SIZE;
				break;
			}
		}

		n = atoi(pc);

		if(n < 0)
			puterr_exit("--crop: invalid value");

		val[pos++] = n;
		pc = pcend;
	}

	g_opt.rcCrop.x1 = val[0];
	g_opt.rcCrop.y1 = val[1];
	g_opt.rcCrop.x2 = val[2];
	g_opt.rcCrop.y2 = val[3];
}

/* --gray */

static void _opt_grayscale(mArgParse *p,char *arg)
{
	g_opt.flags |= OPTION_F_GRAYSCALE;
}

/* --mono */

static void _opt_mono(mArgParse *p,char *arg)
{
	g_opt.flags |= OPTION_F_MONOCHROME;
}

/* --blur : ぼかし */

static void _opt_blur(mArgParse *p,char *arg)
{
	g_opt.blur_type = _getopt_val(arg, 0, BLUR_TYPE_NUM - 1, "blur");
}

/* --addcol : 色加算 */

static void _opt_addcol(mArgParse *p,char *arg)
{
	mStringGetSplitInt(arg, ',', g_opt.addcol, 3);
}

/* --no-reso */

static void _opt_no_reso(mArgParse *p,char *arg)
{
	g_opt.flags |= OPTION_F_NO_RESO;
}

/* --reso */

static void _opt_reso(mArgParse *p,char *arg)
{
	g_opt.fix_reso = atoi(arg);
}

/* --quiet */

static void _opt_quiet(mArgParse *p,char *arg)
{
	g_opt.putmes = 0;
}

/* --version */

static void _opt_version(mArgParse *p,char *arg)
{
	puts(VERSION_TEXT);
	exit_app(0);
}

/* ヘルプ表示 */

static void _put_help(const char *exename)
{
	printf("Usage: %s [option] <files...>\n\n", exename);

	fputs(
"supported format: BMP/PNG/JPEG/TGA/PSD"
#if defined(HAVE_LIB_WEBP)
"/WebP"
#endif
#if defined(HAVE_LIB_TIFF)
"/TIFF"
#endif
#if defined(HAVE_LIB_AVIF)
"/AVIF"
#endif
"/GIF(read only)\n"
"default output: <CURRENT_DIR>/<NAME>_r.<EXT>\n"
"default output size: 100%\n"
"\n"
"Options:\n"
"  -o,--output [FILE]   output filename\n"
"       automatically determine output format from extension.\n"
"  -d,--dir [DIR]       output directory\n"
"  -t,--type [TYPE]     output format (default=png)\n"
"                       bmp/png/jpeg/tga/psd/tiff/webp/avif\n"
"  -f,--fopt [STR]      options for output format\n"
"  -r,--resize [TYPE]   resize type (default=lanczos2)\n"
"       <number, short name, long name>\n"
"       0, mit, mitchell\n"
"       1, lag, lagrange\n"
"       2, lc2, lanczos2\n"
"       3, lc3, lanczos3\n"
"       4, s16, spline16\n"
"       5, s36, spline36\n"
"       6, bic, bicubic\n"
"       7, bl2, blackmansinc2\n"
"       8, bl3, blackmansinc3\n"
"  -s,--size [STR]      output image size ('WIDTHxHEIGHT' or 'N%')\n"
"  -w,--width [N]       output image width (px, height is auto)\n"
"  -h,--height [N]      output image height (px, width is auto)\n"
"  -c,--crop [STR]      crop image\n"
"       'left,top,right,bottom' (4 side crop width)\n"
"       'left,top,WIDTHxHEIGHT'\n"
"  -A,--alpha           output with alpha (100% output only)\n"
"       Image that do not contain alpha will always have alpha.\n"
"       Invalid for JPEG output (normal output).\n"
"       Resizing/blurring is always invalid. Cutting is valid.\n"
"  -B,--bkgnd [HEX]     alpha composite background color\n"
"                       default=FFFFFF(white)\n"
"  -g,--gray            grayscale\n"
"  -m,--mono            monochrome\n"
"  -b,--blur [N]        perform blur before resizing\n"
"       0=none, 1=gauss3x3, 2=gauss5x5\n"
"  -a,--addcol [R,G,B]  add specified value to RGB\n"
"  --no-reso            do not output resolution information\n"
"  --reso [N]           specify resolution (dpi)\n"
"  -q,--quiet           quiet mode\n"
"  -V,--version         print program version\n"
"  --help               display this help\n\n"
"Format options (--fopt):\n"
"  PNG   'l=[0-9]'\n"
"      l = compression level (default=6)\n"
"  JPEG  'q=[0-100],p,s=[hi/mid/low]'\n"
"      q = quality (default=88)\n"
"      p = progressive (default=none)\n"
"      s = sampling factor\n"
"          (hi=4:4:4, mid=4:2:1, low=4:2:0[default])\n"
#if defined(HAVE_LIB_WEBP)
"  WebP  'l=[0-9],q=[0-100],p=[name]'\n"
"      l = lossless format, compression level (default=6)\n"
"      q = lossy format, quality\n"
"      p = lossy preset name (default='default')\n"
"          default,picture,photo,drawing,icon,text\n"
#endif
#if defined(HAVE_LIB_TIFF)
"  TIFF  'c=[none/lzw/deflate/packbits]'\n"
"      c = compression type (default:lzw)\n"
#endif
"  PSD   'c=[0 or 1]'\n"
"      c = compression type (0=none, 1=PackBits)\n"
#if defined(HAVE_LIB_AVIF)
"  AVIF  'l=[0 or 1],q=[0-100],s=[0-9],c=[420/422/444]'\n"
"      l = (0=lossy, 1=lossless) (default=0)\n"
"      q = quality (default=60)\n"
"      s = speed, 0 is slowest (default=5)\n"
"      c = chroma (default=420)\n"
#endif
	, stdout);

	exit_app(0);
}

/* --help */

static void _opt_help(mArgParse *p,char *arg)
{
	_put_help(p->argv[0]);
}

static mArgParseOpt g_opts[] = {
	{"output",'o',1,_opt_output},
	{"dir",'d',1,_opt_dir},
	{"type",'t',1,_opt_type},
	{"fopt",'f',1,_opt_fopt},
	{"resize",'r',1,_opt_resize},
	{"size",'s',1,_opt_size},
	{"width",'w',1,_opt_width},
	{"height",'h',1,_opt_height},
	{"alpha",'A',0,_opt_alpha},
	{"bkgnd",'B',1,_opt_bkgnd},
	{"crop",'c',1,_opt_crop},
	{"gray",'g',0,_opt_grayscale},
	{"mono",'m',0,_opt_mono},
	{"blur",'b',1,_opt_blur},
	{"addcol",'a',1,_opt_addcol},
	{"no-reso",0,0,_opt_no_reso},
	{"reso",0,1,_opt_reso},
	{"quiet",'q',0,_opt_quiet},
	{"version",'V',0,_opt_version},
	{"help",0,0,_opt_help},
	{0,0,0,0}
};

/** オプション取得
 *
 * return: 通常引数の位置 */

int main_get_options(int argc,char **argv)
{
	int ind;
	_argparse dat;

	mMemset0(&dat, sizeof(_argparse));

	dat.ap.argc = argc;
	dat.ap.argv = argv;
	dat.ap.opts = g_opts;

	ind = mArgParseRun(&dat.ap);

	//解析エラー

	if(ind == -1)
		exit_app(1);

	//入力ファイルがない

	if(ind == argc)
		_put_help(argv[0]);

	//出力フォーマット自動判別

	if(g_opt.out_format == FORMAT_AUTO)
	{
		if(mStrIsEmpty(&g_opt.strOutputName))
			//--output がなければ PNG
			g_opt.out_format = FORMAT_PNG;
		else
			_set_outformat();
	}

	//出力フォーマットが非対応のものか (configure の --without-*)

#if !defined(HAVE_LIB_TIFF)
	if(g_opt.out_format == FORMAT_TIFF)
		puterr_exit("unsupported TIFF");
#endif

#if !defined(HAVE_LIB_WEBP)
	if(g_opt.out_format == FORMAT_WEBP)
		puterr_exit("unsupported WebP");
#endif

#if !defined(HAVE_LIB_AVIF)
	if(g_opt.out_format == FORMAT_AVIF)
		puterr_exit("unsupported AVIF");
#endif

	//フォーマットオプション値

	_set_format_opt(dat.fopt);

	return ind;
}
