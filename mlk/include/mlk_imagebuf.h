/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_IMAGEBUF_H
#define MLK_IMAGEBUF_H

#define MLK_IMAGEBUF_GETBUFPT(p,x,y)  ((p)->buf + (y) * (p)->line_bytes + (x) * (p)->pixel_bytes)

typedef struct _mLoadImageOpen mLoadImageOpen;
typedef struct _mLoadImageType mLoadImageType;

struct _mImageBuf
{
	uint8_t *buf;
	int width,
		height,
		pixel_bytes,
		line_bytes;
};

struct _mImageBuf2
{
	uint8_t **ppbuf;
	int width,
		height,
		pixel_bytes,
		line_bytes;
};


#ifdef __cplusplus
extern "C" {
#endif

/* mImageBuf */

mImageBuf *mImageBuf_new(int w,int h,int bits,int line_bytes);
void mImageBuf_free(mImageBuf *p);

mImageBuf *mImageBuf_loadImage(mLoadImageOpen *open,mLoadImageType *type,int bits,int line_bytes);

void mImageBuf_blendColor(mImageBuf *p,mRgbCol col);

/* mImageBuf2 */

mImageBuf2 *mImageBuf2_new(int w,int h,int bits,int line_bytes);
mImageBuf2 *mImageBuf2_new_align(int w,int h,int bits,int line_bytes,int alignment);
void mImageBuf2_free(mImageBuf2 *p);
void mImageBuf2_freeImage(mImageBuf2 *p);

void mImageBuf2_clear0(mImageBuf2 *p);
mlkbool mImageBuf2_crop_keep(mImageBuf2 *p,int left,int top,int width,int height,int line_bytes);

#ifdef __cplusplus
}
#endif

#endif
