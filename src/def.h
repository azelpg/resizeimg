/*$
resizeimg
Copyright (c) 2016-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#define IMAGESIZE_MAX  30000	//画像の最大入出力サイズ


/* 各フォーマットごとの保存オプション */

typedef union
{
	//PNG
	struct
	{
		uint8_t level;
	}png;

	//JPEG
	struct
	{
		uint8_t quality,
			progressive;
		uint16_t sampling;
	}jpg;

	//WebP
	struct
	{
		uint8_t type, //0=可逆、1=非可逆
			level,
			quality,
			preset;
	}webp;

	//TIFF
	struct
	{
		uint8_t comptype;
	}tiff;

	//PSD
	struct
	{
		uint8_t comptype;
	}psd;

	//AVIF
	struct
	{
		uint8_t lossless,
			quality,
			speed;
		uint16_t chroma;
	}avif;
}FormatOpt;


/* オプションデータ */

typedef struct
{
	mStr strOutputDir,	// 出力ディレクトリ (空でなし)
		strOutputName;	// 出力ファイル名 (空でなし)
	int out_format,     // 出力フォーマット
		out_width,		// 出力幅 (0=高さに合わせて自動)
		out_height,		// 出力高さ (0=幅に合わせて自動)
		resize_type,	// リサイズタイプ (default=lanczos2)
		blur_type,      // ぼかしタイプ (default=none)
		fix_reso,		// 解像度を指定値に (0 でなし)
		putmes,			// 0=quiet, 1=メッセージ表示
		addcol[3];      // RGB に加算する値
	uint32_t flags,
		bkgndcol;		// 背景色
	double dscale;		// サイズ: %指定時の倍率
	mRect rcCrop;		// 切り取り幅 (default=すべて 0)
		//OPTION_F_CROP_SIZE が ON で、x2,y2 はサイズ。OFF で切り取り幅
	FormatOpt fopt;		// 各フォーマットのオプション
}OptionData;

extern OptionData g_opt;


/* リサイズ用パラメータ */

typedef struct
{
	double *pweight; //重み
	uint16_t *pindex;
	int srcw,dstw,tap;
}ResizeParam;


/* 作業用データ */

typedef struct
{
	ResizeParam resizex,	//リサイズ用パラメータ。前の画像と同じサイズなら使い回す
		resizey;
	int dpi_x, dpi_y,		//元ファイルの解像度 (DPI)。0 でなし
		prog_curcnt,
		prog_offset,
		prog_curmark,		//経過用、現在のマーク位置 (0-10)
		prog_cnttbl[10];	//経過用、各マークのカウント値
}WorkData;

extern WorkData g_work;


/*---------------------*/

/* 出力フォーマット
 * [!] 順番は変えないこと */

enum
{
	FORMAT_AUTO,	//-o 時、自動判別
	FORMAT_PNG,
	FORMAT_JPEG,
	FORMAT_BMP,
	FORMAT_TIFF,
	FORMAT_WEBP,
	FORMAT_TGA,
	FORMAT_PSD,
	FORMAT_AVIF
};

/* リサイズタイプ */

enum
{
	RESIZE_TYPE_MITCHELL,
	RESIZE_TYPE_LAGRANGE,
	RESIZE_TYPE_LANCZOS2,
	RESIZE_TYPE_LANCZOS3,
	RESIZE_TYPE_SPLINE16,
	RESIZE_TYPE_SPLINE36,
	RESIZE_TYPE_BICUBIC,
	RESIZE_TYPE_BLACKMANSINC2,
	RESIZE_TYPE_BLACKMANSINC3,

	RESIZE_TYPE_NUM
};

/* ぼかしタイプ */

enum
{
	BLUR_TYPE_NONE,
	BLUR_TYPE_GAUSS3x3,
	BLUR_TYPE_GAUSS5x5,

	BLUR_TYPE_NUM
};

/* TIFF 圧縮タイプ */

enum
{
	TIFF_COMP_NONE = 0,
	TIFF_COMP_LZW,
	TIFF_COMP_DEFLATE,
	TIFF_COMP_PACKBITS
};

/* フラグ */

enum
{
	OPTION_F_CROP_SIZE = 1<<0,  //rcCrop x2,y2 は切り取り後のサイズ
	OPTION_F_GRAYSCALE = 1<<1,	//グレイスケール化
	OPTION_F_MONOCHROME = 1<<2,	//2階調化 (優先)
	OPTION_F_ALPHA = 1<<3,		//アルファ付きで保存
	OPTION_F_NO_RESO = 1<<4		//解像度を出力しない
};


/*------------*/

void exit_app(int ret);
void puterr_exit(const char *fmt,...);
void putmes(const char *fmt,...);
void progress_add(int val);
int is_output_alpha(void);
