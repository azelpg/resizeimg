/*$
resizeimg
Copyright (c) 2016-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * リサイズ処理
 **********************************/

#include <math.h>

#include <mlk.h>
#include <mlk_imagebuf.h>
#include <mlk_simd.h>

#include "def.h"


//------------------

#define _ENABLE_SIMD  1

typedef double (*weightfunc_def)(double);

//------------------


/** ResizeParam 解放 */

void ResizeParam_free(ResizeParam *p)
{
	mFree(p->pweight);
	mFree(p->pindex);

	p->pweight = NULL;
	p->pindex = NULL;
}


//======================
// 重み関数
//======================


/** 重み mitchell */

static double _weight_mitchell(double d)
{
	if (d < 1.0)
		return (7.0 / 6.0 * d - 2) * d * d + 8.0 / 9.0;
	else if(d < 2.0)
		return (d - 2) * (d - 2) * (7 * d - 8) / -18;
	else
		return 0;
}

/** 重み lagrange */

static double _weight_lagrange(double d)
{
	if(d < 1.0)
		return (d - 2) * (d + 1) * (d - 1) * 0.5;
	else if(d < 2.0)
		return -(d - 3) * (d - 2) * (d - 1) / 6;
	else
		return 0;
}

/** 重み lanczos2 */

static double _weight_lanczos2(double d)
{
	if(d < MLK_MATH_DBL_EPSILON)
		//d = ほぼ 0。ゼロ除算回避のため
		return 1.0;
	else if(d >= 2.0)
		return 0.0;
	else
	{
		d *= MLK_MATH_PI;
		return sin(d) * sin(d / 2.0) / (d * d / 2.0);
	}
}

/** 重み lanczos3 */

static double _weight_lanczos3(double d)
{
	if(d < MLK_MATH_DBL_EPSILON)
		return 1.0;
	else if(d >= 3.0)
		return 0.0;
	else
	{
		d *= MLK_MATH_PI;
		return sin(d) * sin(d / 3.0) / (d * d / 3.0);
	}
}

/** 重み spline16 */

static double _weight_spline16(double d)
{
	if(d < 1.0)
		return (d - 1) * (5 * d * d - 4 * d - 5) / 5;
	else if(d < 2)
		return (5 * d - 12) * (d - 1) * (d - 2) / -15;
	else
		return 0;
}

/** 重み spline36 */

static double _weight_spline36(double d)
{
	if(d < 1.0)
		return (d - 1) * (247 * d * d - 206 * d - 209) / 209;
	else if(d < 2.0)
		return (19 * d - 45) * (d - 1) * (d - 2) * 6 / -209;
	else if(d < 3.0)
		return (19 * d - 64) * (d - 2) * (d - 3) / 209;
	else
		return 0;
}

/** 重み bicubic */

static double _weight_bicubic(double d)
{
	if(d <= 1.0)
		return (d - 2) * d * d + 1;
	else if(d < 2.0)
		return ((5 - d) * d - 8) * d + 4;
	else
		return 0;
}

/** 重み blackmansinc2 */

static double _weight_blackmansinc2(double d)
{
	if(d < MLK_MATH_DBL_EPSILON)
		return 1.0;
	else if(d >= 2.0)
		return 0;
	else
	{
		//N = 半径 + 1
		// (0.42 + 0.5 * cos(2.0 * PI * d / N) + 0.08 * cos(4.0 * PI * d / N))
		//   * (sin(d * PI) / (d * PI))
	
		d *= MLK_MATH_PI;
	
		return (0.42 + 0.5 * cos(2.0 / 3.0 * d) + 0.08 * cos(4.0 / 3.0 * d))
			* (sin(d) / d);
	}
}

/** 重み blackmansinc3 */

static double _weight_blackmansinc3(double d)
{
	if(d < MLK_MATH_DBL_EPSILON)
		return 1.0;
	else if(d >= 3.0)
		return 0;
	else
	{
		d *= MLK_MATH_PI;
	
		return (0.42 + 0.5 * cos(d * 0.5) + 0.08 * cos(d))
			* (sin(d) / d);
	}
}


//======================
// パラメータ
//======================


/** パラメータバッファ確保
 *
 * return: 0 以外でエラー */

static int _alloc_buf(ResizeParam *p)
{
	mFree(p->pweight);
	mFree(p->pindex);

	p->pweight = (double *)mMalloc(sizeof(double) * p->dstw * p->tap);
	p->pindex = (uint16_t *)mMalloc(2 * p->dstw * p->tap);

	return (!p->pweight || !p->pindex);
}

/** 縮小パラメータセット
 *
 * return: 0 以外でエラー */

static int _set_param_down(ResizeParam *p,
	int srcw,int dstw,int range,weightfunc_def wfunc)
{
	int i,j,pos,tap;
	double *dwork,*pw,dsum,dscale,dscale_rev,d;
	uint16_t *pi;

	//1px あたりの処理ピクセル

	tap = p->tap = (int)((double)srcw / dstw * range * 2 + 0.5);

	//バッファ確保

	if(_alloc_buf(p)) return 1;

	//作業用バッファ

	dwork = (double *)mMalloc(sizeof(double) * tap);
	if(!dwork)
	{
		ResizeParam_free(p);
		return 1;
	}

	//--------------

	pw = p->pweight;
	pi = p->pindex;

	dscale = (double)dstw / srcw;
	dscale_rev = (double)srcw / dstw;

	for(i = 0; i < dstw; i++)
	{
		pos = floor((i - range + 0.5) * dscale_rev + 0.5);
		dsum = 0;

		for(j = 0; j < tap; j++, pi++)
		{
			if(pos < 0)
				*pi = 0;
			else if(pos >= srcw)
				*pi = srcw - 1;
			else
				*pi = pos;

			d = fabs((pos + 0.5) * dscale - (i + 0.5));

			dwork[j] = (wfunc)(d);

			dsum += dwork[j];
			pos++;
		}

		for(j = 0; j < tap; j++)
			*(pw++) = dwork[j] / dsum;
	}

	mFree(dwork);

	return 0;
}

/** 拡大パラメータセット */

static int _set_param_up(ResizeParam *p,
	int srcw,int dstw,int range,weightfunc_def wfunc)
{
	int i,j,tap,npos;
	double *dwork,*pw,dsum,dscale,dpos,dmid;
	uint16_t *pi;

	tap = p->tap = range * 2;

	//バッファ確保

	if(_alloc_buf(p)) return 1;

	//作業用バッファ

	dwork = (double *)mMalloc(sizeof(double) * tap);
	if(!dwork)
	{
		ResizeParam_free(p);
		return 1;
	}
	
	//--------------

	pw = p->pweight;
	pi = p->pindex;

	dscale = (double)srcw / dstw;
	dmid = tap * 0.5 + 0.5;

	for(i = 0; i < dstw; i++)
	{
		dpos = (i + 0.5) * dscale;
		npos = floor(dpos - dmid);
		dpos = npos + 0.5 - dpos;

		dsum = 0;

		for(j = 0; j < tap; j++, pi++)
		{
			if(npos < 0)
				*pi = 0;
			else if(npos >= srcw)
				*pi = srcw - 1;
			else
				*pi = npos;

			dwork[j] = (wfunc)(fabs(dpos));

			dsum += dwork[j];
			dpos += 1.0;
			npos++;
		}

		for(j = 0; j < tap; j++)
			*(pw++) = dwork[j] / dsum;
	}

	mFree(dwork);

	return 0;
}

/** パラメータセット */

static int _set_param(ResizeParam *p,int srcw,int dstw,int range)
{
	weightfunc_def func;
	weightfunc_def funcs[] = {
		_weight_mitchell, _weight_lagrange, _weight_lanczos2,
		_weight_lanczos3, _weight_spline16, _weight_spline36,
		_weight_bicubic, _weight_blackmansinc2, _weight_blackmansinc3
	};

	//前回と同じパラメータなら使い回す

	if(p->pweight && p->pindex && srcw == p->srcw && dstw == p->dstw)
		return 0;

	//新規セット

	p->srcw = srcw;
	p->dstw = dstw;

	func = funcs[g_opt.resize_type];

	if(dstw < srcw)
		return _set_param_down(p, srcw, dstw, range, func);
	else
		return _set_param_up(p, srcw, dstw, range, func);
}


//===========================
// リサイズ
//===========================

/* 水平リサイズ時、切り取りがある場合は、
 * imgsrc の切り取り後の範囲を対象とする */


#if _ENABLE_SIMD && MLK_ENABLE_SSE2
/*---- SSE2 ----*/

/** 水平リサイズ */

static void _resize_horz(ResizeParam *p,mImageBuf2 *imgsrc,mImageBuf2 *imgdst)
{
	uint8_t **ppsrc,**ppdst,*pd,*psy,*ps;
	int ix,iy,i,tap,left;
	uint16_t *pi;
	double *pw;
	__m128d sum1,sum2,c1,c2,wv;
	__m128i ci1,ci2;

	ppsrc = imgsrc->ppbuf + g_opt.rcCrop.y1;
	ppdst = imgdst->ppbuf;
	tap = p->tap;
	left = g_opt.rcCrop.x1;

	for(iy = imgdst->height; iy; iy--)
	{
		pd = *(ppdst++);
		psy = *(ppsrc++);
		pw = p->pweight;
		pi = p->pindex;

		for(ix = imgdst->width; ix; ix--, pd += 4)
		{
			sum1 = _mm_setzero_si128();
			sum2 = sum1;
			
			for(i = tap; i; i--, pw++, pi++)
			{
				ps = psy + ((left + *pi) << 2);

				c1 = _mm_set_pd((double)ps[2], (double)ps[0]);
				c2 = _mm_set_pd(0, (double)ps[1]);
				wv = _mm_load_pd1(pw); //重み

				c1 = _mm_mul_pd(c1, wv); //重みを掛ける
				c2 = _mm_mul_pd(c2, wv);
			
				sum1 = _mm_add_pd(sum1, c1);  //sum に加算
				sum2 = _mm_add_pd(sum2, c2);
			}

			//

			ci1 = _mm_cvtpd_epi32(sum1); //double x2 -> int32 x2 (丸め)
			ci2 = _mm_cvtpd_epi32(sum2);

			ci1 = _mm_unpacklo_epi32(ci1, ci2); //c1[xxBR] c2[xxXG] -> [XBGR]

			ci1 = _mm_packs_epi32(ci1, ci1); //int32 x4 -> int16 x8
			ci1 = _mm_packus_epi16(ci1, ci1); //int16 -> uint8 (飽和)

			_mm_storeu_si32(pd, ci1);
		}

		progress_add(-1);
	}
}

/** 垂直リサイズ */

static void _resize_vert(ResizeParam *p,mImageBuf2 *imgsrc,mImageBuf2 *imgdst)
{
	uint8_t **ppdst,**ppsrc,*pd,*ps;
	int ix,iy,i,tap,xpos;
	uint16_t *pi;
	double *pw;
	__m128d sum1,sum2,c1,c2,wv;
	__m128i ci1,ci2;

	ppsrc = imgsrc->ppbuf;
	ppdst = imgdst->ppbuf;

	pw = p->pweight;
	pi = p->pindex;
	tap = p->tap;

	for(iy = imgdst->height; iy; iy--)
	{
		pd = *(ppdst++);
		xpos = 0;
	
		for(ix = imgdst->width; ix; ix--, pd += 4, xpos += 4)
		{
			sum1 = _mm_setzero_si128();
			sum2 = sum1;

			for(i = 0; i < tap; i++)
			{
				ps = ppsrc[pi[i]] + xpos;
			
				c1 = _mm_set_pd((double)ps[2], (double)ps[0]);
				c2 = _mm_set_pd(0, (double)ps[1]);
				wv = _mm_load_pd1(pw + i); //重み

				c1 = _mm_mul_pd(c1, wv); //重みを掛ける
				c2 = _mm_mul_pd(c2, wv);
			
				sum1 = _mm_add_pd(sum1, c1);  //sum に加算
				sum2 = _mm_add_pd(sum2, c2);
			}

			//

			ci1 = _mm_cvtpd_epi32(sum1);
			ci2 = _mm_cvtpd_epi32(sum2);

			ci1 = _mm_unpacklo_epi32(ci1, ci2);

			ci1 = _mm_packs_epi32(ci1, ci1);
			ci1 = _mm_packus_epi16(ci1, ci1);

			_mm_storeu_si32(pd, ci1);
		}

		pw += tap;
		pi += tap;

		progress_add(-1);
	}
}

#else
/*---- normal ----*/

/** 水平リサイズ */

static void _resize_horz(ResizeParam *p,mImageBuf2 *imgsrc,mImageBuf2 *imgdst)
{
	uint8_t **ppsrc,**ppdst,*pd,*ps,*psy;
	int ix,iy,i,n,tap,left;
	uint16_t *pi;
	double *pw,dw,c[3];

	ppsrc = imgsrc->ppbuf + g_opt.rcCrop.y1;
	ppdst = imgdst->ppbuf;
	tap = p->tap;
	left = g_opt.rcCrop.x1;

	for(iy = imgdst->height; iy; iy--)
	{
		pd = *(ppdst++);
		psy = *(ppsrc++);
		pw = p->pweight;
		pi = p->pindex;

		for(ix = imgdst->width; ix; ix--, pd += 4)
		{
			c[0] = c[1] = c[2] = 0;

			for(i = tap; i; i--, pw++, pi++)
			{
				ps = psy + ((left + *pi) << 2);
				dw = *pw;

				c[0] += ps[0] * dw;
				c[1] += ps[1] * dw;
				c[2] += ps[2] * dw;
			}

			//

			for(i = 0; i < 3; i++)
			{
				n = lround(c[i]);

				if(n < 0) n = 0;
				else if(n > 255) n = 255;

				pd[i] = n;
			}
		}

		progress_add(-1);
	}
}

/** 垂直リサイズ */

static void _resize_vert(ResizeParam *p,mImageBuf2 *imgsrc,mImageBuf2 *imgdst)
{
	uint8_t **ppdst,**ppsrc,*pd,*ps;
	int ix,iy,i,n,tap,xpos;
	uint16_t *pi;
	double *pw,dw,c[3];

	ppsrc = imgsrc->ppbuf;
	ppdst = imgdst->ppbuf;

	pw = p->pweight;
	pi = p->pindex;
	tap = p->tap;

	for(iy = imgdst->height; iy; iy--)
	{
		pd = *(ppdst++);
		xpos = 0;
	
		for(ix = imgdst->width; ix; ix--, pd += 4, xpos += 4)
		{
			c[0] = c[1] = c[2] = 0;

			for(i = 0; i < tap; i++)
			{
				ps = ppsrc[pi[i]] + xpos;
				dw = pw[i];

				c[0] += ps[0] * dw;
				c[1] += ps[1] * dw;
				c[2] += ps[2] * dw;
			}

			//

			for(i = 0; i < 3; i++)
			{
				n = lround(c[i]);

				if(n < 0) n = 0;
				else if(n > 255) n = 255;

				pd[i] = n;
			}
		}

		pw += tap;
		pi += tap;

		progress_add(-1);
	}
}

#endif


//===========================
// main
//===========================


/** リサイズと切り取り処理
 *
 * imgsrc は解放される */

mImageBuf2 *image_resize(mImageBuf2 *imgsrc,int oldw,int oldh,int neww,int newh)
{
	mImageBuf2 *imgdst,*imgtmp;
	int n,range,err = 1;

	imgdst = NULL;

	//半径

	n = g_opt.resize_type;

	if(n == RESIZE_TYPE_LANCZOS3
		|| n == RESIZE_TYPE_SPLINE36
		|| n == RESIZE_TYPE_BLACKMANSINC3)
		range = 3;
	else
		range = 2;

	//水平リサイズ結果用イメージ

	imgtmp = mImageBuf2_new(neww, oldh, 32, 0);
	if(!imgtmp) goto ERR;

	//水平リサイズ

	if(_set_param(&g_work.resizex, oldw, neww, range)) goto ERR;

	_resize_horz(&g_work.resizex, imgsrc, imgtmp);

	//ソースイメージを解放

	mImageBuf2_free(imgsrc);
	imgsrc = NULL;

	//結果イメージ作成

	imgdst = mImageBuf2_new(neww, newh, 32, 0);
	if(!imgdst) goto ERR;

	//垂直リサイズ

	if(_set_param(&g_work.resizey, oldh, newh, range)) goto ERR;

	_resize_vert(&g_work.resizey, imgtmp, imgdst);

	//---------

	err = 0;
	
ERR:
	mImageBuf2_free(imgsrc);
	mImageBuf2_free(imgtmp);
	
	if(err)
	{
		mImageBuf2_free(imgdst);
		imgdst = NULL;
	}

	return imgdst;
}
