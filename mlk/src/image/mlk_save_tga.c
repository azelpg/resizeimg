/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * TGA 保存
 *****************************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_saveimage.h>
#include <mlk_stdio.h>
#include <mlk_util.h>
#include <mlk_imageconv.h>


//---------------

typedef struct
{
	FILE *fp;
	uint8_t *rowbuf;	//Y1行分のバッファ
	int bits,
		pitch;
}tgadata;

//---------------


/** 初期化 */

static mlkerr _init(tgadata *p,mSaveImage *si)
{
	//ビット数

	switch(si->coltype)
	{
		case MSAVEIMAGE_COLTYPE_GRAY:
		case MSAVEIMAGE_COLTYPE_PALETTE:
			p->bits = 8;
			break;
		case MSAVEIMAGE_COLTYPE_RGB:
			p->bits = si->samples_per_pixel * 8;
			break;
		default:
			return MLKERR_INVALID_VALUE;
	}

	//Y1行サイズ

	p->pitch = (p->bits >> 3) * si->width;
		
	//Y1行バッファ
	
	p->rowbuf = (uint8_t *)mMalloc0(p->pitch);
	if(!p->rowbuf) return MLKERR_ALLOC;

	return MLKERR_OK;
}

/** ヘッダ書き込み */

static mlkerr _write_header(tgadata *p,mSaveImage *si)
{
	uint8_t d[18];
	int imgtype,is_palette;

	is_palette = (si->coltype == MSAVEIMAGE_COLTYPE_PALETTE);

	//画像形式

	if(is_palette)
		imgtype = 1;
	else if(si->coltype == MSAVEIMAGE_COLTYPE_RGB)
		imgtype = 2;
	else
		imgtype = 3;

	//ヘッダ

	mSetBuf_format(d, "<bbbhhb4zhhbb",
		0,			//IDフィールドの長さ
		is_palette,	//カラーマップの有無
		imgtype,	//画像形式
		0,			//colmap first index
		(is_palette)? si->palette_num: 0,	//colmap entry num
		(is_palette)? 24: 0,	//colmap bits
		//offset X,Y
		si->width,
		si->height,
		p->bits,
		(p->bits == 32)? 8: 0);	//flags (bit 0-3: アルファ値のビット数)

	if(fwrite(d, 1, 18, p->fp) != 18)
		return MLKERR_IO;

	return MLKERR_OK;
}

/** パレット書き込み */

static mlkerr _write_palette(tgadata *p,mSaveImage *si)
{
	uint8_t *buf,*ps,*pd;
	int i,num,size,ret = MLKERR_OK;

	num = si->palette_num;
	size = num * 3;

	buf = (uint8_t *)mMalloc(size);
	if(!buf) return MLKERR_ALLOC;

	//RGBX => BGR に変換

	ps = si->palette_buf;
	pd = buf;

	for(i = num; i > 0; i--, ps += 4, pd += 3)
	{
		pd[0] = ps[2];
		pd[1] = ps[1];
		pd[2] = ps[0];
	}

	if(fwrite(buf, 1, size, p->fp) != size)
		ret = MLKERR_IO;

	mFree(buf);

	return ret;
}

/** メイン処理 */

static mlkerr _main_proc(tgadata *p,mSaveImage *si)
{
	uint8_t *rowbuf;
	mFuncSaveImageProgress progress;
	int ret,i,height,pitch,last_prog,new_prog;

	//開く

	p->fp = (FILE *)mSaveImage_openFile(si);
	if(!p->fp) return MLKERR_OPEN;

	//初期化

	ret = _init(p, si);
	if(ret) return ret;

	//ヘッダ書き込み

	ret = _write_header(p, si);
	if(ret) return ret;

	//パレット

	if(si->coltype == MSAVEIMAGE_COLTYPE_PALETTE)
	{
		ret = _write_palette(p, si);
		if(ret) return ret;
	}

	//イメージ

	rowbuf = p->rowbuf;
	progress = si->progress;
	height = si->height;
	pitch = p->pitch;
	last_prog = 0;

	for(i = 0; i < height; i++)
	{
		ret = (si->setrow)(si, height - 1 - i, rowbuf, pitch);
		if(ret) return ret;

		//BGR へ変換

		if(p->bits >= 24)
			mImageConv_swap_rb_8(rowbuf, si->width, si->samples_per_pixel);

		//書き込み

		if(fwrite(rowbuf, 1, pitch, p->fp) != pitch)
			return MLKERR_IO;

		//経過

		if(progress)
		{
			new_prog = (i + 1) * 100 / height;

			if(new_prog != last_prog)
			{
				(progress)(si, new_prog);
				last_prog = new_prog;
			}
		}
	}

	//フッタ

	if(mFILEwrite0(p->fp, 8)
		|| fwrite("TRUEVISION-XFILE.\0", 1, 18, p->fp) != 18)
		return MLKERR_IO;

	return MLKERR_OK;
}


//========================


/**@ TGA 保存 */

mlkerr mSaveImageTGA(mSaveImage *si,void *opt)
{
	tgadata dat;
	int ret;

	mMemset0(&dat, sizeof(tgadata));

	ret = _main_proc(&dat, si);

	//

	mSaveImage_closeFile(si, dat.fp);

	mFree(dat.rowbuf);

	return ret;
}
