# resizeimg

http://azsky2.html.xdomain.jp/

画像をリサイズして、各フォーマットで出力。<br>  
※RGB 画像として扱うため、アルファ値は維持できません。

**対応フォーマット**:<br>
BMP/PNG/JPEG/GIF (読み込みのみ)/TIFF/WebP/TGA/PSD/AVIF

**補間方向**:<br>
mitchell/lagrange/lanczos2/lanczos3/spline16/spline32/bicubic/blacmansinc2/blackmansinc3

## 動作環境

Linux、macOS ほか

## コンパイル/インストール

ninja、pkg-config コマンドが必要です。<br>
各開発用ファイルのパッケージも必要になります。ReadMe を参照してください。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~
